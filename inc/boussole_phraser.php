<?php
/**
 * Ce fichier contient les fonctions de phrasage des fichiers XML, JSON ou YAML de description de boussole.
 *
 * @package SPIP\BOUSSOLE\DECODAGE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_BOUSSOLE_FICHIER_EXTENSIONS_DEFAUT')) {
	/**
	 * Extensions possibles pour le fichier de description d'une boussole.
	 * L'extension yaml nécessite l'utilisation du plugin YAML.
	 */
	define(
		'_BOUSSOLE_FICHIER_EXTENSIONS_DEFAUT',
		['xml', 'json']
	);
}

/**
 * Lit un fichier de description de boussole et renvoie les données formatées en tableau.
 * La fonction accepte les fichiers XML, JSON ou YAML.
 *
 * @internal
 *
 * @param string $plugin   Préfixe du plugin fournissant la boussole
 * @param string $boussole Identifiant d'une boussole à acquérir.
 * @param array  &$erreur  Tableau descriptif de l'erreur
 *
 * @return array Données de la boussole lue.
 */
function inc_boussole_phraser_dist(string $plugin, string $boussole, array &$erreur = []) : array {
	// Initialiser les données à retourner et l'erreur éventuellement remontée.
	$donnees = [];

	// Initialisation du bloc d'erreur à ok
	$erreur = [
		'status'  => 200,
		'type'    => 'ok',
		'element' => 'plugin',
		'valeur'  => $plugin,
	];

	// Liste des extensions possibles pour le fichier descriptif d'une boussole
	$extensions = _BOUSSOLE_FICHIER_EXTENSIONS_DEFAUT;
	if (defined('_DIR_PLUGIN_YAML')) {
		$extensions[] = 'yaml';
	}

	// On cherche un fichier de description de la boussole : XML, JSON ou YAML
	$fichier = '';
	foreach ($extensions as $_extension) {
		if ($fichier = find_in_path("boussole-{$boussole}.{$_extension}")) {
			// On précise le bloc d'erreur ok
			$erreur['element'] = "fichier_{$_extension}";
			$erreur['valeur'] = $fichier;
			// On décode le contenu du fichier
			$donnees = $_extension === 'xml'
				? xml_boussole_phraser(file_get_contents($fichier), $boussole, $erreur)
				: json_yaml_boussole_phraser(file_get_contents($fichier), $boussole, strtolower($_extension), $erreur);
			break;
		}
	}

	if (!$fichier) {
		// On précise l'erreur : aucun fichier boussole
		$erreur['status'] = 1010;
		$erreur['type'] = 'fichier_introuvable';
	} elseif (
		$erreur['status'] == 200
		and !empty($donnees['extras'])
	) {
		// Ajouter les traductions, les logos et initialiser les rangs et indicateur d'affichage.
		// -- on parcourt le tableau des extras car il est rangé dans l'ordre de lecture des objets et contient
		//    forcément tous les objets
		include_spip('inc/filtres');
		$rang_groupe = 0;
		$rang_site = 0;
		foreach ($donnees['extras'] as $_cle => $_extra) {
			// Ajouter les traductions
			$traductions = boussole_compiler_traductions($boussole, $_extra['type_objet'], $_extra['aka_objet']);
			$donnees['extras'][$_cle] = array_merge($donnees['extras'][$_cle], $traductions);

			// Ajouter les rangs, logo et l'indicateur d'affichage (toujours à oui)
			if ($_extra['type_objet'] === 'boussole') {
				// -- on identifie le logo de la boussole pour les extras
				$donnees['extras'][$_cle]['logo_objet'] = logo_boussole_identifier($boussole, $_extra['type_objet']);
				// -- dès qu'on détecte la boussole on complète toutes ces informations
				$donnees['boussole']['nom'] = $donnees['extras'][$_cle]['nom_objet'];
				$donnees['boussole']['fournisseur'] = [
					'type'      => 'plugin',
					'id'        => $plugin,
					'extension' => pathinfo($fichier, PATHINFO_EXTENSION)
				];
				$donnees['boussole']['sha'] = sha1_file($fichier);
				$informer = chercher_filtre('info_plugin');
				$donnees['boussole']['version'] = $informer($plugin, 'version', true);
				$donnees['boussole']['logo'] = $donnees['extras'][$_cle]['logo_objet'];
			} elseif ($_extra['type_objet'] === 'groupe') {
				// -- un groupe n'a pas de logo
				$donnees['extras'][$_cle]['logo_objet'] = '';
				// -- dès qu'on détecte un groupe on incrémente son rang et on remet à zéro le rang des sites
				$rang_groupe++;
				$rang_site = 0;
			} elseif ($_extra['type_objet'] === 'site') {
				// -- dès qu'on détecte un site on incrémente son rang et on ajoute les données requises
				$rang_site++;
				$donnees['sites'][$_extra['aka_objet']]['rang_groupe'] = $rang_groupe;
				$donnees['sites'][$_extra['aka_objet']]['rang_site'] = $rang_site;
				$donnees['sites'][$_extra['aka_objet']]['affiche'] = 'oui';
				// Calcul du logo, si il existe
				$donnees['extras'][$_cle]['logo_objet'] = logo_boussole_identifier($boussole, $_extra['type_objet'], $_extra['aka_objet']);
			}
		}
	}

	return $donnees;
}

/**
 * Lit et décode le contenu d'un fichier XML de description de boussole.
 *
 * @internal
 *
 * @param string $contenu  Contenu du fichier XML à phraser
 * @param string $boussole Identifiant d'une boussole à acquérir ou vide si on souhaite acquérir la liste des boussoles.
 * @param array  &$erreur  Tableau descriptif de l'erreur
 *
 * @return array Données lues à partir du contenu XML.
 */
function xml_boussole_phraser(string $contenu, string $boussole, array &$erreur) : array {
	// Initialiser les données à retourner et le bloc d'erreur à ok.
	$donnees = [];

	// Convertir le fichier XML en un objet : si une erreur se produit elle est renvoyée dans un bloc $erreur.
	// -- activer un traitement des erreurs spécifique
	libxml_use_internal_errors(true);
	// -- charger et convertir le fichier XML
	$xml = simplexml_load_string($contenu);

	if ($xml === false) {
		// On récupère la dernière erreur XML
		$erreur_xml = libxml_get_last_error();

		// On complète le bloc d'erreur spécifique
		$erreur['status'] = 1001;
		$erreur['type'] = 'xml_decodage';
		$erreur['extra'] = ['erreur_xml' => $erreur_xml];

		// Vider la liste des erreurs
		libxml_clear_errors();

		// Trace de log
		spip_log("Erreur d'analyse XML pour le fichier `{$erreur['valeur']}` : " . $erreur_xml->message, 'boussole' . _LOG_ERREUR);
	} else {
		// On récupère les attributs de la balise boussole
		// -- on les range dans l'index boussole des informations de la boussole qui seront complétés
		$donnees['boussole'] = xml_boussole_extraire_attributs($xml);
		// -- on constitue aussi les extras de base (hors traductions) de la boussole
		$donnees['extras'][] = [
			'aka_boussole' => $boussole,
			'type_objet'   => 'boussole',
			'aka_objet'    => $boussole
		];
		// -- on initialise l'index des sites
		$donnees['sites'] = [];

		// On parcours l'objet simpleXML par groupe et par site dans les groupes
		foreach ($xml as $_balise_groupe => $_groupe) {
			// On boucle sur les groupes
			if ($_balise_groupe === 'groupe') {
				$attributs_groupe = xml_boussole_extraire_attributs($_groupe);
				// -- un groupe n'a que des extras, pas d'existence en tant qu'objet
				$donnees['extras'][] =
				 [
				 	'aka_boussole' => $boussole,
				 	'type_objet'   => 'groupe',
				 	'aka_objet'    => $attributs_groupe['type']
				 ];

				// On boucle maintenant sur les sites du groupe
				foreach ($_groupe as $_balise_site => $_site) {
					if ($_balise_site === 'site') {
						$attributs_site = xml_boussole_extraire_attributs($_site);
						// On ne récupère que les sites désignés comme actif (rien à voir avec l'affichage)
						if ($attributs_site['actif'] === 'oui') {
							// -- un site existe comme objet : on indexe par l'identifiant pour faciliter la recherche
							$donnees['sites'][$attributs_site['alias']] = [
								'aka_boussole' => $boussole,
								'aka_site'     => $attributs_site['alias'],
								'url_site'     => $attributs_site['src'],
								'aka_groupe'   => $attributs_groupe['type']
							];
							// -- ...et comme extras
							$donnees['extras'][] = [
								'aka_boussole' => $boussole,
								'type_objet'   => 'site',
								'aka_objet'    => $attributs_site['alias']
							];
						}
					}
				}
			}
		}
	}

	return $donnees;
}

/**
 * Extraire les attributs d'une balise XML.
 *
 * @param SimpleXMLElement $xml_element Contenu XML décodé sous la forme d'un objet.
 *
 * @return array Liste des attributs sous la forme [attribut] = valeur.
 */
function xml_boussole_extraire_attributs(SimpleXMLElement $xml_element) : array {
	$attributs = [];

	foreach ($xml_element->attributes() as $_tag => $_valeur) {
		$attributs[$_tag] = (string) $_valeur;
	}

	return $attributs;
}

/**
 * Lit et décode le contenu d'un fichier JSON ou YAML de description de boussole.
 *
 * @internal
 *
 * @param string $contenu   Contenu du fichier JSON ou YAML à phraser
 * @param string $boussole  Identifiant d'une boussole à acquérir ou vide si on souhaite acquérir la liste des boussoles.
 * @param string $extension json ou yaml suivant le fichier source.
 * @param array  $erreur    Tableau descriptif de l'erreur
 *
 * @return array Données lues à partir du contenu JSON ou YAML.
 */
function json_yaml_boussole_phraser(string $contenu, string $boussole, string $extension, array &$erreur) : array {
	// Initialiser les données à retourner et le bloc d'erreur à ok.
	$donnees = [];

	// Convertir le fichier JSON ou YAML : si une erreur se produit elle est renvoyée dans un bloc $erreur.
	if ($extension === 'json') {
		// -- charger et convertir le fichier JSON en gérant les erreurs avec le error handler
		try {
			// Transformation de la chaîne json reçue en tableau associatif
			$contenu_decode = json_decode($contenu, true, 512, JSON_THROW_ON_ERROR);
		} catch (Exception $erreur_json) {
			// On complète le bloc d'erreur
			$erreur['status'] = 1002;
			$erreur['type'] = 'json_decodage';
			$erreur['extra'] = ['erreur_json' => $erreur_json];
			// Trace de log
			spip_log("Erreur d'analyse JSON pour le fichier `{$erreur['valeur']}` : " . $erreur_json->getMessage(), 'boussole' . _LOG_ERREUR);
		}
	} else {
		// -- convertir le fichier YAML et gestion des erreurs sans handler car on ne connait pas la librairie utilisée.
		include_spip('inc/yaml');
		$contenu_decode = yaml_decode($contenu);
		if ($contenu_decode === false) {
			// On complète le bloc d'erreur
			$erreur['status'] = 1003;
			$erreur['type'] = 'yaml_decodage';
			// Trace de log
			spip_log("Erreur d'analyse YAML pour le fichier `{$erreur['valeur']}`", 'boussole' . _LOG_ERREUR);
		}
	}

	if ($erreur['status'] == 200) {
		// -- il faut aller au bout du décodage pour être sur que le fichier est valide
		$schema_invalide = true;
		if (
			!empty($contenu_decode['boussole'])
			and !empty($contenu_decode['groupes'])
		) {
			// -- on range dans l'index boussole des informations de base de la boussole
			$donnees['boussole'] = $contenu_decode['boussole'];
			// -- on constitue aussi les extras de base (hors traductions) de la boussole
			$donnees['extras'][] = [
				'aka_boussole' => $boussole,
				'type_objet'   => 'boussole',
				'aka_objet'    => $boussole
			];
			// -- on initialise l'index des sites
			$donnees['sites'] = [];

			// On parcours le tableau par groupe et par site dans les groupes
			foreach ($contenu_decode['groupes'] as $_groupe) {
				// On boucle sur les groupes
				$attributs_groupe = array_diff_key($_groupe, array_flip(['sites']));
				// -- un groupe n'a que des extras, pas d'existence en tant qu'objet
				$donnees['extras'][] =
				 [
				 	'aka_boussole' => $boussole,
				 	'type_objet'   => 'groupe',
				 	'aka_objet'    => $attributs_groupe['type']
				 ];

				// On boucle maintenant sur les sites du groupe
				if (!empty($_groupe['sites'])) {
					foreach ($_groupe['sites'] as $_site) {
						// On ne récupère que les sites désignés comme actif (rien à voir avec l'affichage)
						if ($_site['actif'] === 'oui') {
							// -- un site existe comme objet : on indexe par l'identifiant pour faciliter la recherche
							$donnees['sites'][$_site['alias']] = [
								'aka_boussole' => $boussole,
								'aka_site'     => $_site['alias'],
								'url_site'     => $_site['src'],
								'aka_groupe'   => $attributs_groupe['type']
							];
							// -- ...et comme extras
							$donnees['extras'][] = [
								'aka_boussole' => $boussole,
								'type_objet'   => 'site',
								'aka_objet'    => $_site['alias']
							];
						}
					}
					$schema_invalide = false;
				}
			}
		}

		if ($schema_invalide) {
			// En cas d'erreur on ne renvoie pas de donnée même partiellement remplie
			$donnees = [];
			// On complète le bloc d'erreur
			$erreur['status'] = 1004;
			$erreur['type'] = "{$extension}_schema";
			// Trace de log
			spip_log("Erreur de schéma {$extension} pour le fichier `{$erreur['valeur']}`", 'boussole' . _LOG_ERREUR);
		}
	}

	return $donnees;
}

/**
 * Identifier le logo de la boussole.
 *
 * @param string      $boussole   Identifiant de la boussole
 * @param string      $type_objet Type d'objet pour lequel on veut récupérer le logo (boussole, site)
 * @param null|string $objet      Identifiant de l'objet (uniquement pour une site)
 *
 * @return string URL Absolue du logo de l'objet concerné (boussole ou site)
 */
function logo_boussole_identifier(string $boussole, string $type_objet, ?string $objet = '') : string {
	// Par défaut, l'objet n'a pas de logo
	$logo = '';

	// On construit le nom de fichier sans extension
	$nom_fichier = 'images/boussole/'
		. $type_objet
		. '-' . $boussole
		. ($objet ? '-' . $objet : '');

	// On cherche un logo SVG d'abord, puis PNG sinon.
	foreach (['svg', 'png'] as $_extension) {
		if ($svg = find_in_path($nom_fichier . '.' . $_extension)) {
			$logo = url_absolue(timestamp($svg));
			break;
		}
	}

	return $logo;
}

/**
 * Compile les traductions du nom, slogan et description d'une boussole, d'un groupe ou d'uns site.
 * La fonction renvoie une balise <multi> pour chaque traduction.
 *
 * @param string $boussole   Identifiant de la boussole
 * @param string $type_objet Type d'objet pour lequel on veut récupérer le logo (boussole, site)
 * @param string $objet      Identifiant de l'objet (boussole, site)
 *
 * @return array Tableau des traductions
 */
function boussole_compiler_traductions(string $boussole, string $type_objet, string $objet) : array {
	$nom = '';
	$slogan = '';
	$description = '';

	if ($fichier_fr = find_in_path("lang/boussole-{$boussole}_fr.php")) {
		// Determination du nom du module, du prefixe et des items de langue
		$item_nom = "nom_{$type_objet}_{$boussole}" . ($type_objet != 'boussole' ? "_{$objet}" : '');
		$item_slogan = "slogan_{$type_objet}_{$boussole}" . ($type_objet != 'boussole' ? "_{$objet}" : '');
		$item_description = "descriptif_{$type_objet}_{$boussole}" . ($type_objet != 'boussole' ? "_{$objet}" : '');

		// On cherche tous les fichiers de langue destines a la traduction du paquet.xml
		if ($fichiers_langue = glob(str_replace('_fr.php', '_*.php', $fichier_fr))) {
			include_spip('inc/lang_liste');
			foreach ($fichiers_langue as $_fichier_langue) {
				$nom_fichier = basename($_fichier_langue, '.php');
				$langue = substr($nom_fichier, strlen("boussole-{$boussole}") + 1 - strlen($nom_fichier));
				// Si la langue est reconnue, on traite la liste des items de langue
				if (isset($GLOBALS['codes_langues'][$langue])) {
					$GLOBALS['idx_lang'] = $langue;
					include("{$_fichier_langue}");
					if (isset($GLOBALS[$langue][$item_nom])) {
						$nom .= "[{$langue}]" . $GLOBALS[$langue][$item_nom];
					}
					if (isset($GLOBALS[$langue][$item_slogan])) {
						$slogan .= "[{$langue}]" . $GLOBALS[$langue][$item_slogan];
					}
					if (isset($GLOBALS[$langue][$item_description])) {
						$description .= "[{$langue}]" . $GLOBALS[$langue][$item_description];
					}
				}
			}
		}
	}

	return [
		'nom_objet'        => "<multi>{$nom}</multi>",
		'slogan_objet'     => "<multi>{$slogan}</multi>",
		'descriptif_objet' => "<multi>{$description}</multi>"
	];
}
