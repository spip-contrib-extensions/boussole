<?php
/**
 * Ce fichier contient l'API de gestion en base de données des boussoles installées sur le site client.
 *
 * @package SPIP\BOUSSOLE\BOUSSOLE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_BOUSSOLE_ID_BOUSSOLE_SPIP')) {
	/**
	 * Liste des types de fournisseur de boussoles : un serveur (REST) ou un plugin de boussole.
	 */
	define('_BOUSSOLE_ID_BOUSSOLE_SPIP', 'spip');
}
if (!defined('_BOUSSOLE_TYPES_FOURNISSEUR')) {
	/**
	 * Liste des types de fournisseur de boussoles : un serveur (REST) ou un plugin de boussole.
	 */
	define(
		'_BOUSSOLE_TYPES_FOURNISSEUR',
		['serveur',	'plugin']
	);
}

/**
 * Mise à jour en base de données d'une liste ou de toutes les boussoles installées.
 *
 * @api
 *
 * @uses boussole_lire_consignation()
 * @uses boussole_identifier_fournisseur()
 * @uses boussole_charger()
 *
 * @param null|array $boussoles Tableau des identifiants des boussoles à actualiser ou tableau vide pour toutes les boussoles
 *
 * @return void
 */
function boussole_actualiser(?array $boussoles = []) : void {
	if (!$boussoles) {
		// On doit actualiser toutes les boussoles
		$boussoles_chargees = boussole_lire_consignation();
	} else {
		// On doit actualiser une liste de boussoles donnée
		$boussoles_chargees = [];
		foreach ($boussoles as $_boussole) {
			if ($consignation = boussole_lire_consignation($_boussole)) {
				$boussoles_chargees[$_boussole] = $consignation;
			}
		}
	}

	if ($boussoles_chargees) {
		foreach ($boussoles_chargees as $_boussole => $_consignation) {
			// Détermination du fournisseur pour accélérer la fonction de chargement
			$fournisseur = boussole_identifier_fournisseur($_consignation);

			// Chargement de la boussole
			$erreur = boussole_charger($_boussole, $fournisseur);
			if ((int) ($erreur['status']) !== 200) {
				spip_log('Actualisation en erreur (boussole = ' . $_boussole . ') : ', 'boussole' . _LOG_ERREUR);
			} else {
				spip_log('Actualisation ok (boussole = ' . $_boussole . ')', 'boussole' . _LOG_INFO);
			}
		}
	}
}

/**
 * Charge en base de donnée une boussole connue par son identifiant.
 * Par défaut, la boussole est récupérée soit depuis un site serveur via une API REST motorisée par REST Factory soit
 * est fournie en local par un plugin.
 *
 * @api
 *
 * @uses boussole_lire_consignation()
 * @uses boussole_identifier_fournisseur()
 * @uses boussole_est_disponible()
 * @uses boussole_acquerir()
 * @uses boussole_phraser()
 * @uses boussole_decharger()
 * @uses boussole_ecrire_consignation()
 *
 * @note
 * 		Le message de retour reflète soit l'ajout ou l'actualisation de la boussole, soit l'erreur
 * 		rencontrée. Les erreurs possibles sont :
 *
 *		- celles retournées par la fonction de phrasage du XML, `phraser_xml_boussole()`,
 *		- une erreur d'écriture en base de données.
 *
 * @param string     $boussole    Identifiant de la boussole
 * @param null|array $fournisseur Type et identifiant du fournisseur. Si omis, la fonction va le rechercher.
 *
 * @return array Bloc de retour dont les index sont comparables à ceux de l'API ezrest. Ainsi, le statut
 *               de retour vaut 200 si tout c'est bien passé.
 */
function boussole_charger(string $boussole, ?array $fournisseur = []) : array {
	// Initialisation à pas d'erreur
	$erreur = [];

	// Récupération de la consignation de la boussole si elle a été déjà chargée
	$consignation = boussole_lire_consignation($boussole);

	// Détermination du fournisseur de la boussole désignée.
	if (!$fournisseur) {
		// Il faut trouver le serveur ou le plugin qui peut fournir la boussole.
		if ($consignation) {
			// -- la boussole est déjà chargée : on récupère directement le fournisseur
			$fournisseur = boussole_identifier_fournisseur($consignation);
		} else {
			// -- la boussole n'est pas chargée : on vérifie sa disponibilité et on récupère son fournisseur le cas échéant.
			boussole_est_disponible($boussole, $fournisseur);
		}
	}

	if (
		isset($fournisseur['type'])
		and in_array($fournisseur['type'], _BOUSSOLE_TYPES_FOURNISSEUR)
		and !empty($fournisseur['id'])
	) {
		// Initialisation de la configuration d'une boussole pour insertion dans la base
		if ($fournisseur['type'] === 'serveur') {
			// On acquiert les données à partir du serveur : celles-ci sont présentées de façon à simplifier
			// l'insertion dans la base.
			$acquerir = charger_fonction('boussole_acquerir', 'inc');
			$configuration = $acquerir($fournisseur['id'], $boussole, $erreur);
		} else {
			// On acquiert la boussole par un fichier descriptif XML, JSON ou YAML. Les données sont présentées
			// comme pour une acquisition REST
			$phraser = charger_fonction('boussole_phraser', 'inc');
			$configuration = $phraser($fournisseur['id'], $boussole, $erreur);
		}

		// Si pas d'erreur : on utilise le statut ok HTTP
		if ($erreur['status'] == 200) {
			// On complete les infos de chaque site
			// - par l'id_syndic si ce site est deja référencé dans la table spip_syndic.
			// - par la configuration de l'affichage si la boussole existe deja dans la base
			foreach ($configuration['sites'] as $_cle => $_site) {
				// -- On recherche l'id_syndic en construisant deux urls possibles : l'une avec / l'autre sans.
				//    Si le site n'existe pas on force l'id_syndic à 0 car ce peut-être une raz, le site ayant été supprimé
				$urls = [
					$_site['url_site'],
					(substr($_site['url_site'], -1, 1) == '/') ? substr($_site['url_site'], 0, -1) : $_site['url_site'] . '/'
				];
				$configuration['sites'][$_cle]['id_syndic'] = 0;
				$where = [
					sql_in('url_site', $urls),
					'statut=' . sql_quote('publie')
				];
				if ($id_syndic = sql_getfetsel('id_syndic', 'spip_syndic', $where)) {
					$configuration['sites'][$_cle]['id_syndic'] = (int) $id_syndic;
				}

				// -- On recherche une configuration d'affichage (si elle existe)
				$select = [
					'rang_groupe',
					'rang_site',
					'affiche'
				];
				$where = [
					'aka_boussole=' . sql_quote($configuration['boussole']['alias']),
					'aka_site=' . sql_quote($_site['aka_site'])
				];
				if ($affichage = sql_fetsel($select, 'spip_boussoles', $where)) {
					$configuration['sites'][$_cle]['rang_groupe'] = (int) ($affichage['rang_groupe']);
					$configuration['sites'][$_cle]['rang_site'] = (int) ($affichage['rang_site']);
					$configuration['sites'][$_cle]['affiche'] = $affichage['affiche'];
				}
			}

			// On insere les sites et les extras de la boussole en base
			// -- début de la transaction
			if (sql_preferer_transaction()) {
				sql_demarrer_transaction();
			}

			// -- suppression au prealable des données de la boussole si elle existe déjà en base
			if ($consignation) {
				boussole_decharger($boussole);
			}

			// -- insertion de la nouvelle liste de sites pour cette boussole
			sql_insertq_multi('spip_boussoles', $configuration['sites']);
			// -- insertion de la nouvelle liste des extras pour cette boussole
			sql_insertq_multi('spip_boussoles_extras', $configuration['extras']);

			// -- consignation des informations de mise a jour de la boussole
			$configuration['boussole']['nbr_sites'] = count($configuration['sites']);
			$configuration['boussole']['maj'] = date('Y-m-d H:i:s');
			boussole_ecrire_consignation($boussole, $configuration['boussole']);

			// -- cloture de la transaction
			if (sql_preferer_transaction()) {
				sql_terminer_transaction();
			}

			// On definit le bloc de retour
			// -- pas d'erreur donc on renvoie le statut 200
			$erreur = [
				'status'  => 200,
				'type'    => $consignation ? 'boussole_maj' : 'boussole_ajout',
				'element' => 'boussole',
				'valeur'  => $boussole
			];
		}
	}

	return $erreur;
}

/**
 * Suppression de la boussole dans la base de données du site client.
 *
 * @api
 *
 * @uses boussole_ecrire_consignation()
 *
 * @param string $boussole Identifiant de la boussole
 *
 * @return bool `false` si l'identifiant de la boussole est vide, `true` sinon
 */
function boussole_decharger(string $boussole) : bool {
	// Par défaut, la suppression se passe mal
	$retour = false;

	if ($boussole) {
		// Début de la transaction
		if (sql_preferer_transaction()) {
			sql_demarrer_transaction();
		}

		// On supprime les sites de cette boussole
		sql_delete('spip_boussoles', 'aka_boussole=' . sql_quote($boussole));

		// On supprime les extras de cette boussole
		$trouver = charger_fonction('trouver_table', 'base');
		if ($trouver('spip_boussoles_extras')) {
			sql_delete('spip_boussoles_extras', 'aka_boussole=' . sql_quote($boussole));
		}

		// On supprime ensuite la meta consignant la derniere mise a jour de cette boussole
		boussole_ecrire_consignation($boussole);

		// Cloture de la transaction
		if (sql_preferer_transaction()) {
			sql_terminer_transaction();
		}

		// Tout c'est bien passé
		$retour = true;
	}

	return $retour;
}

/**
 * Renvoie les informations de consignation d'une ou de toutes les boussoles chargées.
 * La fonction lit les metas directement dans la base sans passer par le cache metas.
 *
 * @api
 *
 * @param null|string $boussole Identifiant de la boussole ou vide pour toutes les boussoles
 *
 * @return array Les informations de consignation d'une boussole ou de toutes les boussoles.
 */
function boussole_lire_consignation(?string $boussole = '') : array {
	// Par défaut, on considère la boussole comme non chargée en base
	$consignations = [];

	// On vérifie l'existence de la meta associée à la boussole
	if ($boussole) {
		// On lit la configuration de la meta de façon classique
		include_spip('inc/config');
		$consignations = lire_config("boussole_infos_{$boussole}", []);
	} else {
		// On est obligé de lire la base pour avoir toutes les metas
		$where = [
			'nom LIKE ' . sql_quote('boussole_infos_%')
		];
		$metas = sql_allfetsel('valeur', 'spip_meta', $where);
		if ($metas) {
			$metas = array_column($metas, 'valeur');
			// On désérialise le tableau des boussoles et on l'index par l'identifant de la boussole
			$consignations = array_column(
				array_map('unserialize', $metas),
				null,
				'alias'
			);
		}
	}

	return $consignations;
}

/**
 * Consigne un chargement de boussole ou l'efface.
 *
 * @api
 *
 * @param string     $boussole     Identifiant de la boussole
 * @param null|array $consignation Tableau de consignation du chargement de la boussole. Si vide ou non fourni
 *                                 provoque l'effacement de la consignation.
 *
 * @return bool `true` si ok, `false` sinon (identifiant de boussole vide)
 */
function boussole_ecrire_consignation(string $boussole, ?array $consignation = []) : bool {
	// Par défaut, on considère la boussole comme non chargée en base
	$retour = false;

	// On vérifie l'existence de la meta associée à la boussole
	if ($boussole) {
		include_spip('inc/config');
		if ($consignation) {
			// On veut écrire une consignation
			ecrire_config("boussole_infos_{$boussole}", $consignation);
		} else {
			// On veut effacer une consignation
			effacer_config("boussole_infos_{$boussole}");
		}
		$retour = true;
	}

	return $retour;
}

/**
 * Renvoie le fournisseur sous sa forme tabulaire à partir de la consignation d'une boussole.
 * Cette fonction gère la transition entre le nouveau format du fournisseur et l'ancien.
 *
 * @api
 *
 * @param array $consignation Tableau des informations de consignation d'une boussole
 *
 * @return array Tableau des informations sur le fournisseur.
 */
function boussole_identifier_fournisseur(array $consignation) : array {
	// Par défaut, on considère la boussole comme non chargée en base
	$fournisseur = [];

	// On vérifie l'existence de la meta associée à la boussole
	if ($consignation) {
		if (!empty($consignation['fournisseur'])) {
			$fournisseur = $consignation['fournisseur'];
		} elseif (!empty($consignation['serveur'])) {
			$fournisseur['type'] = 'serveur';
			$fournisseur['id'] = $consignation['serveur'];
		} elseif (!empty($consignation['plugin'])) {
			$fournisseur['type'] = 'plugin';
			$fournisseur['id'] = $consignation['plugin'];
		}
	}

	return $fournisseur;
}

/**
 * Détermine si une boussole est disponible soit par API REST auprès d'un serveur ou soit via un plugin
 * installé localement.
 * On privilégie le serveur qui est réputé plus à jour.
 *
 * @api
 *
 * @uses boussole_lister_disponibilites()
 *
 * @param string $boussole    Identifiant de la boussole
 * @param array  $fournisseur Tableau identifiant le fournisseur:
 *                            - type : serveur ou plugin
 *                            - id   : l'identifiant du serveur ou le préfixe du plugin
 *
 * @return bool `true` si la boussole est disponible, `false` sinon. Le fournisseur est retourné dans l'argument
 *              de sortie $fournisseur
 */
function boussole_est_disponible(string $boussole, array &$fournisseur) : bool {
	// Par défaut la boussole n'est pas disponible
	$est_disponible = false;

	if ($boussole) {
		// Récupérer les boussoles disponibles
		$boussoles = boussole_lister_disponibilites();

		// On scrute d'abord les serveurs qui sont réputés être à jour et ensuite les plugins locaux.
		foreach (_BOUSSOLE_TYPES_FOURNISSEUR as $_type_fournisseur) {
			$index_type_fournisseur = "{$_type_fournisseur}s";
			if (
				!$est_disponible
				and !empty($boussoles[$index_type_fournisseur])
			) {
				foreach ($boussoles[$index_type_fournisseur] as $_id_fournisseur => $_boussoles) {
					if (isset($_boussoles[$boussole])) {
						$est_disponible = true;
						$fournisseur = [
							'type' => $_type_fournisseur,
							'id'   => $_id_fournisseur
						];
						break;
					}
				}
			}
		}
	}

	return $est_disponible;
}

/**
 * Renvoie, pour le site courant, la liste des boussoles disponibles au chargement.
 * La fonction détermine les boussoles accessibles par serveur et celles mise à disposition par des plugins
 * activés.
 *
 * @api
 *
 * @uses serveur_boussole_lister_disponibilites()
 * @uses boussole_lire_consignation()
 * @uses boussole_compiler_traductions()
 *
 * @return array Tableau des boussoles disponibles. Pour chaque boussole on indique le fournisseur, serveur ou plugin.
 */
function boussole_lister_disponibilites() : array {
	// Mise en cache statique des boussoles
	static $boussoles = [];

	if (!$boussoles) {
		// Déterminer les serveurs accessibles
		include_spip('inc/serveur_boussole');
		if ($serveurs = serveur_boussole_lister_disponibilites()) {
			// Lister les boussoles servies par chaque serveur
			$acquerir = charger_fonction('boussole_acquerir', 'inc');
			foreach (array_keys($serveurs) as $_serveur) {
				$erreur = [];
				if (
					$boussoles_serveur = $acquerir($_serveur, '', $erreur)
					and ((int) ($erreur['status']) === 200)
				) {
					$boussoles['serveurs'][$_serveur] = $boussoles_serveur['boussoles'];
				}
			}
		}

		// Déterminer les boussoles fournies par un plugin installé sur le site courant
		include_spip('inc/boussole_phraser');
		$boussoles_plugin = pipeline('declarer_boussoles', []);
		foreach ($boussoles_plugin as $_boussole => $_plugin) {
			$consignation = boussole_lire_consignation($_boussole);
			if ($consignation) {
				// Si la boussole a déjà été chargée on renvoie sa consignation
				$boussoles['plugins'][$_plugin['prefixe']][$_boussole] = $consignation;
			} else {
				// Si la boussole n'est pas encore chargée on renvoie les informations possibles dans le même format
				// qu'une consignation
				// -- traduction du nom
				$traductions = boussole_compiler_traductions($_boussole, 'boussole', $_boussole);
				// -- version de la boussole
				$informer = chercher_filtre('info_plugin');
				$version = $informer($_plugin['prefixe'], 'version', true);
				$boussoles['plugins'][$_plugin['prefixe']][$_boussole] = [
					'alias'       => $_boussole,
					'nom'         => $traductions['nom_objet'],
					'version'     => $version,
					'fournisseur' => [
						'type' => 'plugin',
						'id'   => $_plugin['prefixe']
					]
				];
			}
		}
	}

	return $boussoles;
}
