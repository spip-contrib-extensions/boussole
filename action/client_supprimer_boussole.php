<?php
/**
 * Ce fichier contient l'action `client_supprimer_boussole` utilisée par un site client pour
 * supprimer de façon sécurisée une boussole donnée.
 *
 * @package SPIP\BOUSSOLE\CLIENT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Cette action permet au site client de supprimer de sa base de données, de façon sécurisée,
 * une boussole donnée.
 *
 * Cette action est réservée aux webmestres.
 * Elle nécessite un seul argument, l'identifiant de la boussole.
 *
 * @uses boussole_decharger()
 *
 * @return void
 */
function action_client_supprimer_boussole_dist() : void {
	// Securisation et autorisation car c'est une action auteur:
	// -> argument attendu est l'alias de la boussole
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$boussole = $securiser_action();

	// Verification des autorisations
	if (!autoriser('ajouterdansclient', 'boussole')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Suppression de la boussole connue par son alias
	if ($boussole) {
		include_spip('inc/boussole');
		boussole_decharger($boussole);

		spip_log('ACTION SUPPRIMER BOUSSOLE : alias = ' . $boussole, 'boussole' . _LOG_INFO);
	}
}
