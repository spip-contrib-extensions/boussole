<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Activation du CRON de mise à jour des statistiques de plugins.
 *
 * @param array $taches_generales Liste des taches à compléter
 *
 * @return array Liste des taches mise à jour
 */
function boussole_taches_generales_cron(array $taches_generales) : array {
	// Ajout des taches CRON de mise a jour périodique (toutes les 24h) :
	// - des boussoles ajoutés pour le client
	$taches_generales['boussole_actualiser_client'] = 24 * 3600;

	return $taches_generales;
}
