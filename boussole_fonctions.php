<?php
/**
 * Ce fichier contient les balises et les filtres fournis par le plugin.
 *
 * @package SPIP\BOUSSOLE\Squelettes
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
// ----------------------- Balises propres à Boussole ---------------------------------

/**
 * Compilation de la balise `#BOUSSOLE_INFOS` retournant les informations générales sur une
 * boussole.
 *
 * La balise #BOUSSOLE_INFOS renvoie :
 *
 * - le tableau des infos contenues dans la meta boussole_infos_xxx si l'alias "xxx" est fourni,
 * - la liste de tous les tableaux d'infos des meta boussole_infos_* sinon.
 *
 * La liste des informations disponibles est la suivante :
 *
 * - 'logo' : l'url du logo de la boussole
 * - 'version' : la version de la boussole
 * - 'fournisseur' : le fournisseur de la boussole
 * - 'sha' : sha256 du fichier cache de la boussole
 * - 'alias' : alias de la boussole
 * - 'demo' : url de la page de démo de la boussole
 * - 'nbr_sites' : nombre de sites intégrés dans la boussole
 * - 'maj' : date de la dernière mise à jour des informations
 *
 * @api
 *
 * @balise
 *
 * @uses calcul_boussole_infos()
 *
 * @example
 * 		`#BOUSSOLE_INFOS{spip}|table_valeur{logo}` renvoie l'url du logo de la boussole "spip"
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_BOUSSOLE_INFOS(Champ $p) : Champ {
	$alias_boussole = interprete_argument_balise(1, $p);
	$alias_boussole = isset($alias_boussole) ? str_replace('\'', '"', $alias_boussole) : '""';

	$p->code = 'calcul_boussole_infos(' . $alias_boussole . ')';

	return $p;
}

/**
 * Récupération des informations sur une boussole donnée ou sur toutes les boussoles installées.
 *
 * Les informations retournées pour une boussole d'alias "xxx" sont celles stockées dans la meta
 * `boussole_infos_xxx` auxquelles on adjoint la date de la dernière mise à jour de cette meta.
 *
 * @param string $boussole Alias de la boussole ou chaine vide
 *
 * @return array Tableau de la ou des boussoles installées.
 *               Si l'alias de la boussole est erroné, la fonction retourne un tableau vide
 */
function calcul_boussole_infos(string $boussole) : array {
	// La balise renvoie exactement le resultat de la fonction boussole_lire_consignation().
	include_spip('inc/boussole');

	return boussole_lire_consignation($boussole);
}

// ----------------------- Filtres propres à Boussole ---------------------------------

/**
 * Traduction d'un champ d'une boussole, d'un groupe de sites ou d'un site.
 *
 * @api
 *
 * @filtre
 *
 * @param string      $boussole Alias de la boussole
 * @param string      $champ    Champ à traduire. La liste des champs possibles est :
 *                              - 'nom_boussole', 'slogan_boussole', 'descriptif_boussole' pour un objet boussole
 *                              - 'nom_groupe', 'slogan_groupe' pour un objet groupe
 *                              - 'nom_site', 'slogan_site', 'descriptif_site', 'nom_slogan_site' pour un objet site
 * @param null|string $objet    Identifiant d'un objet groupe ou site. Vide pour la traduction d'un champ d'un objet
 *                              boussole
 *
 * @return string Champ traduit dans la langue du site
 */
function boussole_traduire(string $boussole, string $champ, ?string $objet = '') : string {
	// Liste des champs possibles
	static	$champs_autorises = [
		'nom_boussole', 'slogan_boussole', 'descriptif_boussole',
		'nom_groupe', 'slogan_groupe',
		'nom_site', 'slogan_site', 'descriptif_site', 'nom_slogan_site'
	];

	// Par défaut, en cas d'erreur on renvoie une traduction vide
	$traduction = '';

	// Détermination de la traduction à rechercher dans les extras de boussole
	if (
		$boussole
		and ($champ !== '')
	) {
		if (in_array($champ, $champs_autorises)) {
			if ($champ === 'nom_slogan_site') {
				$type_objet = 'site';
				$aka_objet = $objet;
				$select = ['nom_objet', 'slogan_objet'];
			} else {
				[$type_champ, $type_objet] = explode('_', $champ);
				if ($type_objet === 'boussole') {
					$aka_objet = $boussole;
				} else {
					$aka_objet = $objet;
				}
				$select = "{$type_champ}_objet";
			}

			// Accès à la table boussoles_extras où sont stockées les traductions
			$where = [
				'aka_boussole=' . sql_quote($boussole),
				'type_objet=' . sql_quote($type_objet),
				'aka_objet=' . sql_quote($aka_objet)];
			if ($traductions = sql_fetsel($select, 'spip_boussoles_extras', $where)) {
				if (count($traductions) === 1) {
					$traduction = extraire_multi($traductions[$select]);
				} elseif (count($traductions) === 2) {
					$traduction = extraire_multi($traductions['nom_objet']) . '-' . extraire_multi($traductions['slogan_objet']);
				}
			}
		}
	}

	return $traduction;
}

/**
 * Renvoie chemin nom du logo topnav à partir du chemin du logo de la boussole.
 * Le nom du logo est en fait suffixé par -topnav.
 *
 * @api
 *
 * @filtre
 *
 * @param string $logo URL du logo de boussole.
 *
 * @return string URL du logo topnav de la même boussole.
 */
function boussole_logo_topnav(string $logo) : string {
	$logo_topnav = $logo;
	if ($logo) {
		$informations = pathinfo($logo);
		$logo_topnav = $informations['dirname'] . '/'
			. $informations['filename'] . '-topnav'
			. '.' . $informations['extension'];
	}

	return $logo_topnav;
}
