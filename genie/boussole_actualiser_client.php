<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce CRON permet mettre à jour les boussoles.
 *
 * @uses boussole_actualiser()
 *
 * @param int $last Timestamp de la date de dernier appel de la tâche.
 *
 * @return int Retour de la fonction toujours à 1
 */
function genie_boussole_actualiser_client_dist($last) {
	include_spip('inc/boussole');
	boussole_actualiser();

	return 1;
}
