<?php
/**
 * Ce fichier contient la configuration des caches du plugin.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches gérés par Boussole.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array Tableau de la configuration brute du plugin.
 */
function boussole_cache_configurer(string $plugin) : array {
	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Cache.
	// -- Pas de cache pour l'index des collections
	return [
		'rest' => [
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => false,
			'nom_prefixe'     => 'rest',
			'nom_obligatoire' => ['serveur'],
			'nom_facultatif'  => ['boussole'],
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => true,
			'separateur'      => '-',
			'conservation'    => 3600 * 24
		],
	];
}
