<?php
/**
 * Ce fichier contient la fonction surchargeable de récupération des informations d'un plugin.
 *
 * @package SPIP\BOUSSOLE\SITE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Remplace ou ajoute un logo au site. L'image source est connue par son url.
 *
 * @param int    $id_site   Id du site concerné.
 * @param string $mode      Le type de l'icone, "on" pour le logo de base, ou "off" pour le survol.
 * @param string $url_image Url de l'image source destinée à devenir le logo du site.
 *
 * @throws JsonException
 *
 * @return void
 */
function inc_iconifier_site_dist(int $id_site, string $mode, string $url_image) : void {
	include_spip('inc/distant');
	$fichier = _DIR_RACINE . copie_locale($url_image, 'force');

	include_spip('action/editer_logo');
	logo_modifier('site', $id_site, $mode, $fichier);
}
