<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire permet d'activer la fonction serveur et d'en saisir l'identifiant.
 *
 * @return array Le tableau des données à charger par le formulaire et l'indicateur éditable qui est mis à false
 *               si le plugin REST Factory n'est pas actif.
 */
function formulaires_configurer_boussole_serveur_charger() : array {
	// Chargement des données de configuration déjà en meta
	include_spip('inc/cvt_configurer');
	$valeurs = cvtconf_formulaires_configurer_recense('configurer_boussole_serveur');

	// Le site peut devenir un serveur si le plugin REST Factory est actif
	$valeurs['editable'] = defined('_DIR_PLUGIN_EZREST');

	return $valeurs;
}

/**
 * Vérification des saisies : si on active la fonction serveur, il est obligatoire de saisir un identifiant et
 * que celui-ci soit différent du serveur spip si on est sur un site différent.
 *
 * @uses serveur_boussole_est_spip()
 *
 * @return array
 */
function formulaires_configurer_boussole_serveur_verifier() : array {
	// Initialisation des erreurs de saisie
	$erreurs = [];

	// On vérifie, si on demande d'activer la fonction serveur :
	// -- que le nom est bien saisi,
	// -- que le nom est bien formé (word)
	// -- et que si il vaut l'identifiant du serveur spip, que le site courant est bien celui du serveur spip
	if (_request('actif')) {
		$nom = _request('nom');
		if (!$nom) {
			$erreurs['nom'] = _T('info_obligatoire');
		} elseif (!preg_match('#^[a-z0-9]{4,}$#', $nom)) {
			$erreurs['nom'] = _T('boussole:erreur_nok_serveur_nom_incorrect', ['valeur' => $nom]);
		} elseif (
			include_spip('inc/serveur_boussole')
			and serveur_boussole_est_spip($nom)
			and !serveur_boussole_est_spip()
		) {
			$erreurs['nom'] = _T('boussole:erreur_nok_serveur_nom_spip');
		}
	}

	return $erreurs;
}
