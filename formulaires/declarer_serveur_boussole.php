<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérification des saisies :
 * - l'url est non vide,
 * - bien formée,
 * - ne correspond pas à celui de spip
 * - et que le serveur désigné fourni bien au moins une boussole.
 *
 * @uses serveur_boussole_est_spip()
 * @uses erreur_boussole_expliquer()
 *
 * @return array
 */
function formulaires_declarer_serveur_boussole_verifier_dist() : array {
	// Initialisation des erreurs
	$erreurs = [];

	// On vérifie :
	// -- l'url est non vide,
	// -- bien formée,
	// -- ne correspond pas à celui de spip
	// -- et que le serveur désigné fourni bien au moins une boussole
	include_spip('inc/erreur_boussole');
	$url = _request('url');
	if (!$url) {
		$erreurs['url'] = _T('info_obligatoire');
	} elseif (!tester_url_absolue($url)) {
		$erreurs['url'] = _T('boussole:erreur_nok_serveur_url_incorrecte', ['valeur' => $url]);
	} elseif (
		include_spip('inc/serveur_boussole')
		and serveur_boussole_est_spip($url)
	) {
		$erreurs['url'] = _T('boussole:erreur_nok_serveur_url_spip');
	} else {
		// Vérifier que le serveur fournit au moins une boussole
		$erreur_acquisition = [];
		$acquerir = charger_fonction('boussole_acquerir', 'inc');
		$boussoles = $acquerir($url, '', $erreur_acquisition);
		if ((int) ($erreur_acquisition['status']) !== 200) {
			if (isset($erreur_acquisition['titre'])) {
				// Si le serveur a renvoyé une erreur expliqué on l'affiche directement
				$erreurs['url'] = $erreur_acquisition['titre'];
			} else {
				$erreurs['url'] = erreur_boussole_expliquer($erreur_acquisition);
			}
		} elseif (!$boussoles['boussoles']) {
			$erreurs['url'] = _T('boussole:erreur_nok_serveur_vide');
		} else {
			// On stocke l'identifiant du serveur de façon à l'utiliser dans le traitement
			set_request('serveur', $boussoles['serveur']['id']);
		}
	}

	return $erreurs;
}

/**
 * Exécution de l'action du formulaire : déclaration du serveur de boussoles.
 *
 * @uses serveur_boussole_declarer()
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_declarer_serveur_boussole_traiter_dist() : array {
	// Initialisation du retour du formulaire
	$retour = [];

	// Récupération de l'url et de l'id du serveur
	$url = _request('url');
	$serveur = _request('serveur');

	// Ajout du serveur dans la liste des serveurs disponibles pour le site client courant
	include_spip('inc/serveur_boussole');
	serveur_boussole_declarer($serveur, $url);

	// Renvoie du message d'ajout
	$retour['message_ok'] = _T('boussole:erreur_ok_serveur_ajout', ['id' => $serveur, 'url' => $url]);
	$retour['editable'] = true;

	spip_log("Ajout serveur `{$serveur}` ok (url : `{$url}`)", 'boussole' . _LOG_INFO);

	return $retour;
}
