<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_actualiser_boussoles' => 'Aktualizovať kompasy',
	'bouton_importer' => 'Nahrať',
	'bouton_importer_boussole' => 'Nahrať stránky kompasu',
	'bouton_retirer_boussole' => 'Odstrániť kompas',
	'bouton_retirer_serveur' => 'Odstrániť server',
	'bouton_supprimer' => 'Odstrániť',
	'bouton_synchroniser' => 'Synchronizovať údaje',
	'bulle_afficher' => 'Zobraziť v šablónach',
	'bulle_aller_site' => 'Prejsť na odkazovanú stránku',
	'bulle_cacher' => 'Nezobrazovať v šablónach',
	'bulle_deplacer_bas' => 'Posunúť nadol',
	'bulle_deplacer_haut' => 'Posunúť nahor',

	// C
	'colonne_alias' => 'Prezývka',
	'colonne_description_cache' => 'Opis',
	'colonne_fichier_cache' => 'Cache',
	'colonne_nbr_sites' => 'Obsahuje',
	'colonne_prefixe_plugin' => 'Zásuvný modul ?',
	'colonne_serveur' => 'Server',
	'colonne_titre' => 'Titulok',
	'colonne_url' => 'URL',
	'colonne_version' => 'Verzia',

	// D
	'description_noisette_boussole' => 'Štandardné zobrazenie kompasu. Môžete si vybrať spôsob zobrazenia (textové odkazy, logá, atď.) a jeho presné nastavenia',
	'description_noisette_boussole_actualite' => 'Zobrazenie syndikovaných článkov zo stránok kompasu podľa šablóny zobrazenia <code>boussole_liste_actualite.</code>',
	'description_noisette_boussole_contenu_z' => 'Zobrazenie všetkých informácií o kompase ako hlavný obsah stránky Z a o spôsobe zobrazenia <code>boussole_contenu_z.</code>',
	'description_noisette_boussole_fil_ariane' => 'Zobrazenie mikronavigácie kompasu.',
	'description_page_boussole' => 'Stránka s podrobnými údajmi o kompase',

	// I
	'info_activite_serveur' => 'V predvolených nastaveniach nie je funkcia "server" zásuvného modulu aktivovaná. Môžete ju aktivovať zvolením príslušných nastavení a priradením názvu ',
	'info_ajouter_boussole' => 'Pridaním kompasov do svojej databázy budete môcť využívať šablóny na zobrazenie verejne prístupných stránok.<br /> Ak už kompas existuje, tento formulár ho aktualizuje s tým, že nastavenia zobrazenia ponechá bezo zmeny.',
	'info_ajouter_serveur' => 'Tento formulár vám umožňuje zadať server s kompasmi. Podľa predvolených nastavení sa dá stále dostať na server „spip“   z klientských stránok.',
	'info_boussole_manuelle' => 'Príručka ku Kompasu',
	'info_configurer_boussole' => 'Tento formulár vám umožňuje nastaviť zobrazenie kompasu na vybraných stránkach tak, že si vyberiete, v akom poradí a v akej skupine sa stránky majú alebo nemajú zobraziť. Stránky, ktoré sa nebudú zobrazovať, budú označené tieňom v pozadí a sivým písmom.',
	'info_rubrique_parent' => 'Na vytváranie stránok kompasu si musíte vybrať titulnú rubriku.',
	'info_site_boussole' => 'Táto stránka je súčasťou kompasu:',
	'info_site_boussoles' => 'Táto stránka je súčasťou kompasov:',
	'info_url_serveur' => 'Zadajte adresu stránky servera.',

	// L
	'label_1_boussole' => '@nb@ kompas',
	'label_1_site' => '@nb@ stránka',
	'label_a_class' => 'Trieda so záložkou (kotvou) loga',
	'label_activite_serveur' => 'Aktivovať funkciu server ?',
	'label_actualise_le' => 'Aktualizovaný',
	'label_affiche' => 'Zobrazí sa?',
	'label_afficher_descriptif' => 'Zobraziť opisy stránok?',
	'label_afficher_lien_accueil' => 'Zobraziť odkaz na úvodnú stránku?',
	'label_afficher_slogan' => 'Zobraziť slogany stránok?',
	'label_alias_boussole' => 'Prezývka kompasu',
	'label_ariane_separateur' => 'Oddeľovač:',
	'label_boussole' => 'Kompas, ktorý sa má zobraziť',
	'label_cartouche_boussole' => 'Zobraziť rám kompasu?',
	'label_demo' => 'Nájde demo stránku tohto kompasu na adrese',
	'label_descriptif' => 'Opis',
	'label_div_class' => 'Trieda globálneho divu',
	'label_div_id' => 'Id globálneho divu',
	'label_langue_site' => 'Pre preložené údaje nahrajte len preklad do jazyka stránku.',
	'label_li_class' => 'Trieda každého tagu li v zozname',
	'label_logo' => 'Logo',
	'label_max_articles' => 'Maximálny počet zobrazených článkov na stránku',
	'label_max_sites' => 'Maximálny počet stránok',
	'label_mode' => 'Vyberte si kompas',
	'label_mode_standard' => '"@boussole@", oficiálny kompas stránok SPIPu',
	'label_modele' => 'Spôsob zobrazenia',
	'label_n_boussoles' => '@nb@ kompasov',
	'label_n_sites' => '@nb@ stránok',
	'label_nom' => 'Názov',
	'label_nom_serveur' => 'Názov servera',
	'label_p_class' => 'Trieda odseku pri opise',
	'label_publier_import' => 'Automaticky publikovať nové vytvorené stránky. Pred nahrávaním sa stav stránok nemení.',
	'label_sepia' => 'Kód sépiovej farby (sans #)',
	'label_slogan' => 'Slogan',
	'label_taille_logo' => 'Maximálna veľkosť loga (v pixeloch)',
	'label_taille_logo_boussole' => 'Maximálna veľkosť loga kompasu (v pixeloch)',
	'label_taille_titre' => 'Maximálna veľkosť nadpisu kompasu',
	'label_titre_actualite' => 'Zobraziť názov bloku s novinkami?', # MODIF
	'label_titre_boussole' => 'Zobraziť nadpis kompasu?',
	'label_titre_groupe' => 'Zobraziť názov skupiny?',
	'label_titre_site' => 'Zobraziť názvy stránok?',
	'label_type_bulle' => 'Informácia zobrazená v bublinkovej nápovedi každého odkazu',
	'label_type_description' => 'Opis zobrazený vedľa loga',
	'label_ul_class' => 'Trieda tagu ul v zozname',
	'label_url' => 'URL',
	'label_url_serveur' => 'URL servera',
	'label_version' => 'Verzia',

	// M
	'message_nok_0_site_importe' => 'Z kompasu @boussole@ nebola nahratá žiadna stránka.',
	'message_nok_alias_boussole_manquant' => 'Prezývka kompasu nebola serveru "@serveur@" poskytnutá.',
	'message_nok_aucune_boussole_hebergee' => 'Na serveri "@serveur@" sa stále nenachádza žiaden kompas.',
	'message_nok_boussole_inconnue' => 'Žiaden kompas nemá prezývku "@alias@"',
	'message_nok_boussole_non_hebergee' => 'Kompas "@alias@" sa nenachádza na serveri "@serveur@".',
	'message_nok_cache_boussole_indisponible' => 'Dočasný súbor kompasu «@alias@» nie je na serveri «@serveur@» dostupný.',
	'message_nok_cache_liste_indisponible' => 'Dočasný súbor týchto kompasov nie je na serveri "@serveur@" dostupný.',
	'message_nok_declaration_boussole_xml' => 'Manuálny kompas "@boussole@" sa nedá deklarovať, lebo sa nedá nájsť jeho súbor XML.',
	'message_nok_ecriture_bdd' => 'Chyba pri zápise do databázy
(tabuľka @table@).',
	'message_nok_reponse_invalide' => 'Odpoveď servera "@serveur@" má nesprávnu podobu alebo zadanú adresu nemá žiaden aktívny server.', # MODIF
	'message_ok_1_site_importe' => 'Jedna stránka bola nahraná z kom­pasu @boussole@.',
	'message_ok_boussole_actualisee' => 'Kompas "@fichier@" bol aktualizovaný.',
	'message_ok_boussole_ajoutee' => 'Kompas "@fichier@" bol pridaný.',
	'message_ok_boussole_manuelle_ajoutee' => 'Na serveri ste deklarovali manuálny kompas "@boussole@" a dočasné pamäte boli aktualizované.',
	'message_ok_n_sites_importes' => '@nb@ stránok bolo nahraných z kom­pasu @boussole@.',
	'message_ok_serveur_ajoute' => 'Server "@serveur@" bol pridaný (@url@).',
	'modele_boussole_liste_avec_logo' => 'Zoznam odkazov s názvami, logami a opisom',
	'modele_boussole_liste_par_groupe' => 'Zoznam textových odkazov podľa skupiny',
	'modele_boussole_liste_simple' => 'Jednoduchý zoznam textových odkazov',
	'modele_boussole_panorama' => 'Galéria log',
	'modele_boussole_panorama_sepia' => 'Galéria log s efektom sépia',

	// O
	'onglet_client' => 'Funkcia Klient', # MODIF
	'onglet_configuration' => 'Nastavenia zásuvného modulu',
	'onglet_serveur' => 'Funkcia Server', # MODIF
	'option_aucune_description' => 'Žiaden opis',
	'option_descriptif_site' => 'Opis stránky',
	'option_nom_site' => 'Názov stránky',
	'option_nom_slogan_site' => 'Názov a slogan stránky',
	'option_slogan_site' => 'Slogan stránky',

	// T
	'titre_boite_autres_boussoles' => 'Ďalšie kompasy',
	'titre_boite_infos_boussole' => 'KOMPAS PREZÝVKY',
	'titre_boite_logo_boussole' => 'LOGO KOMPASU',
	'titre_form_ajouter_boussole' => 'Pridať alebo aktualizovať kompas',
	'titre_form_ajouter_serveur' => 'Pridať server kompasov',
	'titre_form_configurer_serveur' => 'Nastaviť funkciu server', # MODIF
	'titre_formulaire_configurer' => 'Nastavenia zobrazenia kompasu',
	'titre_liste_boussoles' => 'Zoznam dostupných kompasov',
	'titre_liste_serveurs' => 'Zoznam prístupných serverov zo stránky',
	'titre_page_boussole' => 'Riadenie kompasov',
	'titre_page_configurer' => 'Nastavenia zásuvného modulu Kompas',
	'titre_page_importer_boussole' => 'Nahrávanie kompasu',
	'titre_page_serveurs_boussole' => 'Funkcia Server', # MODIF
];
