<?php
/**
 * Ce fichier contient l'action `client_actualiser_boussoles` utilisée par un site client pour
 * actualiser l'ensemble des boussoles installées.
 *
 * @package SPIP\BOUSSOLE\CLIENT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Cette action permet au site client d'actualiser l'ensemble des boussoles insérées dans
 * sa base de données.
 *
 * L'action est utilisée sur demande depuis la page d'administration du client de l'espace privé.
 *
 * @uses boussole_actualiser()
 *
 * @return void
 */
function action_client_actualiser_boussoles_dist() : void {
	// Securisation: aucun argument attendu, néanmoins étant donné le bug de la balise

	// Verification des autorisations
	if (!autoriser('ajouterdansclient', 'boussole')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Actualisation de toutes les boussoles installées sur le site client
	include_spip('inc/boussole');
	boussole_actualiser();
}
