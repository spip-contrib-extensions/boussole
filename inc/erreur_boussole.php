<?php
/**
 * Ce fichier contient les fonctions d'API des serveurs de boussole.
 *
 * @package SPIP\BOUSSOLE\ERREUR
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie le message de retour ou d'erreur compilé à partir du tableau d'erreur fourni en argument.
 *
 * @api
 *
 * @param array $erreur Tableau descriptif de l'erreur
 *
 * @return string Message d'erreur compilé à partir de l'erreur.
 */
function erreur_boussole_expliquer(array $erreur) : string {
	// Retourner un message vide si on ne peut pas compiler le message
	$message = '';

	if (
		$erreur
		and isset($erreur['status'], $erreur['type'])
	) {
		// -- le raccourci de l'erreur
		$item = 'erreur_' . ((int) ($erreur['status']) === 200 ? 'ok_' : 'nok_') . $erreur['type'];

		// -- les paramètres du raccourci
		$parametres = [
			'status' => $erreur['status']
		];
		if (isset($erreur['valeur'])) {
			$parametres['valeur'] = $erreur['valeur'];
		}
		if (!empty($erreur['extra'])) {
			$parametres = array_merge($parametres, $erreur['extra']);
		}

		// -- la traduction de l'erreur
		$message = _T("boussole:{$item}", $parametres);
	}

	return $message;
}
