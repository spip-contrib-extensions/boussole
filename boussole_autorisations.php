<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param $flux
 *
 * @return mixed
 */
function boussole_autoriser($flux) {
	return $flux;
}

/**
 * Autorisation minimale d'utiliser le plugin Boussole.
 * - il faut être admin complet
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('defaut');
}

/**
 * Autorisation de voir la liste des boussoles et des serveurs.
 * - il faut avoir l'autorisation minimale d'utiliser la Boussole
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussoles_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('boussole');
}

/**
 * Autorisation de voir la page d'une boussole.
 * - il faut avoir l'autorisation minimale d'utiliser la Boussole
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('boussole');
}

/**
 * Autorisation d'ajout d'un serveur dans la liste des serveurs accessibles par le site client.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_declarerserveur_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('boussole');
}

/**
 * Autorisation de retrait d'un serveur dans la liste des serveurs accessibles par le site client.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_retirerserveur_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/serveur_boussole');

	return autoriser('boussole')
		and !serveur_boussole_est_spip($id);
}

/**
 * Autorisation d'ajout d'une boussole dans la base d'un client.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_ajouterdansclient_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('boussole');
}

/**
 * Autorisation de modifier l'affichage d'une boussole, à savoir, l'ordre des groupes et des sites, la visibilité
 * des sites.
 * Cette configuration est associée à la boussole pour le serveur et sera renvoyée aux sites client demandeurs.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_modifierdansserveur_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('boussole');
}

/**
 * Autorisation d'importer les sites d'une boussole dans la liste des sites référencés.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_importer_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/config');
	$activation = lire_config('activer_sites');

	return autoriser('boussole')
		and ($activation === 'oui');
}

/**
 * Autorisation pour un site de répondre aux requêtes des clients.
 * Pour cela:
 * - le plugins REST Factory doit être activé
 * - le mode serveur doit être actif sur le site.
 *
 * @param string $faire Action demandée
 * @param string $type  Type d'objet sur lequel appliquer l'action
 * @param int    $id    Identifiant de l'objet
 * @param array  $qui   Description de l'auteur demandant l'autorisation
 * @param array  $opt   Options de cette autorisation
 *
 * @return bool true s'il a le droit, false sinon
**/
function autoriser_boussole_servir_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/config');
	$serveur_actif = lire_config('boussole/serveur/actif', '') === 'on';

	return $serveur_actif
		and defined('_DIR_PLUGIN_EZREST');
}
