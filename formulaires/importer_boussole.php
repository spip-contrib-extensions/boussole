<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire propose la liste des boussoles accessibles
 * à partir des serveurs que le site client a déclaré.
 *
 * @return array Le tableau des données à charger par le formulaire :
 */
function formulaires_importer_boussole_charger_dist() : array {
	return [
		'boussole'               => 'spip',
		'id_parent'              => 0,
		'langue_site'            => 1,
		'importer_statut_publie' => 0,
	];
}

/**
 * Vérification des saisies : si on active la fonction serveur, il est obligatoire de saisir un identifiant et
 * que celui-ci soit différent du serveur spip si on est sur un site différent.
 *
 * @return array Le tableau des messages d'erreur ou vide si ok.
 */
function formulaires_importer_boussole_verifier_dist() : array {
	$erreurs = [];

	if (!_request('id_parent')) {
		$erreurs['id_parent'] = _T('info_obligatoire');
	}

	return $erreurs;
}

/**
 * Exécution de l'action du formulaire : les sites de la boussole choisie sont importés dans la base.
 *
 * @uses boussole_actualiser()
 * @uses importer_sites_boussole()
 * @uses boussole_traduire()
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_importer_boussole_traiter_dist() : array {
	$retour = [];

	$id_parent = (int) (_request('id_parent'));
	$langue_site = (bool) (_request('langue_site'));
	$forcer_statut_publie = (bool) (_request('importer_statut_publie'));
	$boussole = _request('boussole');

	// Actualiser la boussole avant l'importation.
	// En effet, si des modifications sauvages ont été faites sur la base de données il se peut
	// que l'id_syndic soit encore présent dans spip_boussoles alors que le site a disparu de spip_syndic.
	include_spip('inc/boussole');
	boussole_actualiser([$boussole]);
	// Importer les sites de la boussole
	$nb_sites = importer_sites_boussole($boussole, $id_parent, $langue_site, $forcer_statut_publie);
	// Actualiser la boussole (en fait uniquement les id_syndic) maintenant que les sites référencés sont créés.
	boussole_actualiser([$boussole]);

	if (!$nb_sites) {
		$retour['message_erreur'] = _T('boussole:message_nok_0_site_importe', ['boussole' => boussole_traduire($boussole, 'nom_boussole')]);
	} else {
		$retour['message_ok'] = singulier_ou_pluriel(
			$nb_sites,
			'boussole:message_ok_1_site_importe',
			'boussole:message_ok_n_sites_importes',
			'nb',
			['boussole' => boussole_traduire($boussole, 'nom_boussole')]
		);
	}
	$retour['editable'] = true;

	return $retour;
}

/**
 * Importe les sites d'une boussole dans une rubrique donnée.
 *
 * @param string    $boussole             Identifiant de la boussole
 * @param int       $id_parent            Id de la rubrique parente
 * @param null|bool $langue_site          Indique que seule la langue du site est importée
 * @param null|bool $forcer_statut_publie Indique que le statut des sites créés est forcé à publié
 *
 * @return int Nombre de sites importés.
 */
function importer_sites_boussole(string $boussole, int $id_parent, ?bool $langue_site = true, ?bool $forcer_statut_publie = false) : int {
	$nb_sites = 0;

	if ($id_parent) {
		$from = ['spip_boussoles as b', 'spip_boussoles_extras as x'];
		$select = ['b.url_site', 'b.id_syndic', 'x.nom_objet', 'x.slogan_objet', 'x.descriptif_objet', 'x.logo_objet'];
		$where = [
			'b.aka_boussole=' . sql_quote($boussole),
			'b.aka_boussole=x.aka_boussole', 'b.aka_site=x.aka_objet',
			'x.type_objet=' . sql_quote('site')];
		$sites = sql_allfetsel($select, $from, $where);

		if ($sites) {
			include_spip('action/editer_site');
			include_spip('inc/filtres');
			foreach ($sites as $_site) {
				// Nouveau site : il faut le créer préalablement dans la rubrique choisie
				$id_syndic = !$_site['id_syndic'] ? site_inserer($id_parent) : $_site['id_syndic'];

				if ($id_syndic) {
					// Mise à jour complète du site existant ou venant d'être créé
					$contenu = [
						'url_site'   => $_site['url_site'],
						'nom_site'   => ($langue_site ? extraire_multi($_site['nom_objet']) : $_site['nom_objet']),
						'date'       => date('Y-m-d H:i:s'),
						'descriptif' => ($langue_site ? extraire_multi($_site['descriptif_objet']) : $_site['descriptif_objet'])];
					if (!$_site['id_syndic']) {
						$contenu = array_merge($contenu, ['statut' => (($forcer_statut_publie and autoriser('publierdans', 'rubrique', $id_parent)) ? 'publie' : 'prop')]);
					}
					$erreur = site_modifier($id_syndic, $contenu);

					if (!$erreur) {
						if ($_site['logo_objet']) {
							// Mise à jour du logo du site normal ("on").
							$iconifier = charger_fonction('iconifier_site', 'inc');
							$iconifier($id_syndic, 'on', $_site['logo_objet']);
						}

						$nb_sites++;
					} elseif (!$_site['id_syndic']) {
						// On traite l'erreur en supprimant le site si celui-ci vient d'être inséré
						sql_delete('spip_syndic', 'id_syndic=' . sql_quote($id_syndic));
					}
				}
			}
		}
	}

	return $nb_sites;
}
