<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-boussole?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// B
	'boussole_description' => 'Installa questo plugin sul tuo sito e avrai accesso, nei tuoi template, a tutti i siti della galassia SPIP utilizzando
modelli ecc...
_ Per gli appassionati del "fai da te", puoi anche utilizzare questo plugin per visualizzare il tuo elenco di siti e progettare le tue visualizzazioni.',
	'boussole_nom' => 'Bussola',
	'boussole_slogan' => 'I migliori indirizzi nella galassia SPIP o altrove!',
];
