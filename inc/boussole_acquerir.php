<?php
/**
 * Ce fichier contient la fonction d'acquisition d'une boussole via une API REST.
 *
 * @package SPIP\BOUSSOLE\BOUSSOLE\CHARGEMENT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Acquiert une boussole ou la liste des boussoles d'un serveur.
 * La fonction utilise un cache pour éviter de requêter le serveur à chaque appel.
 *
 * @api
 *
 * @uses tester_url_absolue()
 * @uses identifiant_slug()
 * @uses cache_est_valide()
 * @uses cache_lire()
 * @uses cache_ecrire()
 * @uses boussole_requeter()
 *
 * @param string $serveur  Identifiant ou URL du serveur
 * @param string $boussole Identifiant d'une boussole à acquérir ou vide si on souhaite acquérir la liste des boussoles.
 * @param array  &$erreur  Tableau descriptif de l'erreur
 *
 * @return array Configuration d'une boussole ou liste des boussoles d'un serveur.
 */
function inc_boussole_acquerir_dist(string $serveur, string $boussole, array &$erreur) : array {
	// Initialiser les données à retourner et l'erreur éventuellement remontée.
	$donnees = [];

	// Initialisation du bloc d'erreur à ok : le serveur peut-être identifié par son nom ou son URL
	$erreur = [
		'status'  => 200,
		'type'    => 'ok',
		'element' => 'serveur',
		'valeur'  => $serveur,
	];

	// Construction du fichier cache
	// -- si le serveur est désigné par son URL on slugifie l'URL pour en faire une identifiant de cache
	include_spip('inc/ezcache_cache');
	$cache = [
		'type'    => 'rest',
		'serveur' => tester_url_absolue($serveur)
			? identifiant_slug($serveur)
			: $serveur,
	];
	if ($boussole) {
		$cache['boussole'] = $boussole;
	}

	// Test de la validité du cache et renvoie du chemin complet
	if ($fichier_cache = cache_est_valide('boussole', 'rest', $cache)) {
		// Lecture des données du fichier cache valide
		$donnees = cache_lire('boussole', 'rest', $fichier_cache);
	} else {
		// Appel à l'API REST du serveur
		$reponse = boussole_requeter($serveur, $boussole);
		if ((int) ($reponse['erreur']['status']) === 200) {
			// On ne renvoie que les données
			$donnees = $reponse['donnees'];

			// On complète les informations de la boussole ou des boussoles avec le fournisseur car c'est le point
			// de vue du client qui est utile : vu du client toutes les boussoles sont fournies par le serveur
			// -- on gère en outre le cas où le serveur est désigné par son URL : l'id est dans la réponse
			$fournisseur = [
				'type' => 'serveur',
				'id'   => $donnees['serveur']['id']
			];
			if ($boussole) {
				$donnees['boussole']['fournisseur'] = $fournisseur;
			} else {
				// On boucle sur chaque boussole
				foreach ($donnees['boussoles'] as $_id_boussole => $_description_boussole) {
					$donnees['boussoles'][$_id_boussole]['fournisseur'] = $fournisseur;
				}
			}

			// On met à jour le cache
			cache_ecrire('boussole', 'rest', $cache, json_encode($donnees));
		} else {
			// On renvoie le bloc d'erreur, les données restent vides.
			$erreur = $reponse['erreur'];
		}
	}

	return $donnees;
}

/**
 * Renvoie, à partir de l'identifiant du serveur et de l'identifiant de la boussole, le tableau des données demandées.
 * Le service utilise dans ce cas une chaine JSON qui est décodée pour fournir
 * le tableau de sortie. Le flux retourné par le service est systématiquement
 * transcodé dans le charset du site avant d'être décodé.
 *
 * @internal
 *
 * @uses tester_url_absolue()
 * @uses serveur_boussole_lister_disponibilites()
 * @uses recuperer_url()
 *
 * @param string      $serveur  Identifiant ou URL du serveur
 * @param null|string $boussole Identifiant d'une boussole à acquérir ou vide si on souhaite acquérir la liste des boussoles.
 *
 * @return array Tableau de la réponse. Si la réponse est ok, le bloc d'erreur n'est pas fourni
 */
function boussole_requeter(string $serveur, ?string $boussole = '') : array {
	// Calcul de l'url complète
	if (tester_url_absolue($serveur)) {
		// On a passé l'URL de base du serveur et pas son id
		$url_serveur = $serveur;
	} else {
		// On a passé l'id du serveur : on va chercher son URL de base en configuration
		include_spip('inc/serveur_boussole');
		$serveurs = serveur_boussole_lister_disponibilites();
		$url_serveur = $serveurs[$serveur]['url'] ?? '';
	}
	// -- URL complète de la requête
	$url = rtrim($url_serveur, '/') . '/http.api/ezrest/boussoles' . ($boussole ? '/' . $boussole : '');

	// Options de la fonction de récupération du JSON
	include_spip('inc/distant');
	$options = [
		'transcoder' => true,
	];

	// Acquisition du flux de données
	$flux = recuperer_url($url, $options);

	$reponse = [];
	if (
		!$flux
		or ($flux['status'] === 404)
		or empty($flux['page'])
	) {
		// Erreur serveur 501 (Not Implemented)
		$reponse['erreur'] = [
			'status'  => 501,
			'type'    => 'serveur_api_indisponible',
			'element' => 'serveur',
			'valeur'  => $serveur,
			'extra'   => ['url' => $url_serveur]
		];
		// Trace de log
		spip_log("API indisponible : {$url}", 'boussole' . _LOG_ERREUR);
	} else {
		try {
			// Transformation de la chaîne json reçue en tableau associatif
			$reponse = json_decode($flux['page'], true, 512, JSON_THROW_ON_ERROR);
		} catch (Exception $erreur) {
			// Erreur serveur 520 (Unknown Error)
			$reponse['erreur'] = [
				'status'  => 520,
				'type'    => 'serveur_reponse_json',
				'element' => 'serveur',
				'valeur'  => $serveur,
				'extra'   => [
					'url'         => $url_serveur,
					'erreur_json' => $erreur->getMessage()
				]
			];
			// Trace de log
			spip_log("Erreur d'analyse JSON pour l'URL `{$url}` : " . $reponse['erreur']['extra']['erreur_json'], 'boussole' . _LOG_ERREUR);
		}
	}

	return $reponse;
}
