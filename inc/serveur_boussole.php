<?php
/**
 * Ce fichier contient les fonctions d'API des serveurs de boussole.
 *
 * @package SPIP\BOUSSOLE\SERVEUR\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_BOUSSOLE_ID_SERVEUR_SPIP')) {
	/**
	 * Liste des types de fournisseur de boussoles : un serveur (REST) ou un plugin de boussole.
	 */
	define('_BOUSSOLE_ID_SERVEUR_SPIP', 'spip');
}

if (!defined('_BOUSSOLE_URL_SERVEUR_SPIP')) {
	/**
	 * Liste des types de fournisseur de boussoles : un serveur (REST) ou un plugin de boussole.
	 */
	define('_BOUSSOLE_URL_SERVEUR_SPIP', 'https://boussole.spip.net');
}

/**
 * Renvoie, pour le site courant, son identifiant de serveur de boussoles.
 * Le site doit être configuré comme serveur de boussole.
 *
 * @api
 *
 * @return string Identifiant du serveur ou vide si le site n'est pas configuré comme un serveur
 */
function serveur_boussole_identifier() : string {
	// Lire le nom du serveur dans la meta du plugin Boussole
	include_spip('inc/config');

	return lire_config('boussole/serveur/nom', '');
}

/**
 * Détermine si le serveur connu par son identifiant ou par son url est le serveur officiel de SPIP fournbissant
 * la boussole SPIP.
 *
 * @api
 *
 * @param null|string $serveur Identifiant ou URL du serveur à vérifier. Si une URL est passée à la fonction, celle-ci
 *                             considère qu'elle est, a priori, bien formée.
 *                             Si l'identifiant est vide, la fonction utilise l'adresse du site courant
 *
 * @return bool true si le serveur correspond à celui de spip ou false sinon.
 */
function serveur_boussole_est_spip(?string $serveur = '') : bool {
	if (!$serveur) {
		// On veut comparer le site courant au serveur spip : ona compare l'adresse du site courant
		include_spip('inc/config');
		$serveur = lire_config('adresse_site');
	}

	if (tester_url_absolue($serveur)) {
		$est_spip = rtrim(strtolower($serveur), '/') === _BOUSSOLE_URL_SERVEUR_SPIP;
	} else {
		$est_spip = strtolower($serveur) === _BOUSSOLE_ID_SERVEUR_SPIP;
	}

	return $est_spip;
}

/**
 * Ajoute un serveur et son url dans la liste des serveurs disponibles, donc accessibles par l'API REST.
 *
 * @api
 *
 * @param string $serveur Identifiant du serveur
 * @param string $url     URL de base du serveur (adresse du site)
 *
 * @return bool `true` si la déclaration est ok, `false` sinon (par exemple, si le serveur ou l'url est vide).
 */
function serveur_boussole_declarer(string $serveur, string $url) : bool {
	// On renvoie false par défaut
	$retour = false;

	if (
		$serveur
		and $url
	) {
		// Déterminer les serveurs accessibles
		$serveurs = serveur_boussole_lister_disponibilites();
		$serveurs[$serveur] = [
			'url' => $url
		];

		// Ajout du serveur dans la liste
		include_spip('inc/config');
		ecrire_config('boussole/client/serveurs_disponibles', $serveurs);
		$retour = true;
	}

	return $retour;
}

/**
 * Retire une serveur désigné par son identifiant de la liste des serveurs accessibles par le site courant.
 * Il n'est pas possible de supprimer le serveur officiel spip.
 *
 * @api
 *
 * @param string $serveur Identifiant du serveur
 *
 * @return bool `true` si le serveur, `false` sinon (par exemple, si le serveur est spip ou l'identifiant est erronné).
 */
function serveur_boussole_retirer(string $serveur) : bool {
	// On renvoie false par défaut
	$retour = false;

	// On renvoie une erreur si l'identifiant est spip ou est vide
	if (
		$serveur
		and !serveur_boussole_est_spip($serveur)
	) {
		// Liste des serveurs disponibles
		$serveurs = serveur_boussole_lister_disponibilites();

		// On vérifie que le serveur existe dans la liste
		if (isset($serveurs[$serveur])) {
			// Retrait du serveur de la liste
			unset($serveurs[$serveur]);
			ecrire_config('boussole/client/serveurs_disponibles', $serveurs);
			$retour = true;

			spip_log('ACTION RETRAIT SERVEUR : alias = ' . $serveur, 'boussole' . _LOG_INFO);
		}
	}

	return $retour;
}

/**
 * Renvoie, pour le site courant, la liste des serveurs déclarés, donc disponibles via l'API REST.
 *
 * @api
 *
 * @return array Tableau des serveurs disponibles. Pour chaque serveur on renvoie l'url de base.
 */
function serveur_boussole_lister_disponibilites() : array {
	// Mise en cache statique des serveus
	static $serveurs = [];

	if (!$serveurs) {
		// Déterminer les serveurs accessibles
		include_spip('inc/config');
		$serveurs = lire_config('boussole/client/serveurs_disponibles', []);
	}

	return $serveurs;
}
