<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_actualiser_boussoles' => 'Atualisà li boussola',
	'bouton_importer' => 'Empourtà',
	'bouton_importer_boussole' => 'Empourtà lu sit d’una boussola',
	'bouton_retirer_boussole' => 'Retirà la boussola',
	'bouton_retirer_serveur' => 'Retirà lou servidou',
	'bouton_supprimer' => 'Suprimà',
	'bouton_synchroniser' => 'Sincronisà li dounada',
	'bulle_afficher' => 'Afichà en lu moudel',
	'bulle_aller_site' => 'Anà soubre la pàgina dóu sit referençat',
	'bulle_cacher' => 'Noun afichà en lu moudel',
	'bulle_deplacer_bas' => 'Desplaçà en per avau',
	'bulle_deplacer_haut' => 'Desplaçà en per amount',

	// C
	'colonne_alias' => 'Alias',
	'colonne_description_cache' => 'Descricioun',
	'colonne_fichier_cache' => 'Amagadou',
	'colonne_nbr_sites' => 'Counten',
	'colonne_prefixe_plugin' => 'Plugin ?',
	'colonne_serveur' => 'Servidou',
	'colonne_titre' => 'Titre',
	'colonne_url' => 'URL',
	'colonne_version' => 'Versioun',

	// D
	'description_noisette_boussole' => 'Afichage estandart d’una boussola. Poudès chausì lou moudel d’afichage (estac testual, logou...) couma pura la siéu counfiguracioun prechisa',
	'description_noisette_boussole_actualite' => 'Afichage dei article sindicat dei sit d’una boussola segoun lou moudel d’afichage <code>boussole_liste_actualite</code>.',
	'description_noisette_boussole_contenu_z' => 'Afichage de touti li infourmacioun d’une boussola couma countengut  principal d’una pàgina Z e segoun lou moudel d’afichage <code>boussole_contenu_z</code>.',
	'description_noisette_boussole_fil_ariane' => 'Afichage dóu fiéu d’Ariana d’una boussola.',
	'description_page_boussole' => 'Pàgina dei infourmacioun detaiadi d’una boussola',

	// I
	'info_activite_serveur' => 'En mancança, la founcioun servidou dóu plugin noun es ativada. Poudès l’ativà en chausissent l’oupcioun courrespoundenta aquì souta e en lì atribuissent un noum.',
	'info_ajouter_boussole' => 'En ajustant dei boussola a la vouòstra basa de dounada, aurès la poussibilità d’utilisà lu moudel fournit per lu afichà en li vouòstri pàgina pùbliqui.<br />Se la boussola esista jà, aqueu fourmulari permetra de la metre a jou en counservant la counfiguracioun d’afichage.',
	'info_ajouter_serveur' => 'Aqueu fourmulari vi permete de declarà un servidou de boussola. En mancança, lou servidou « spip » es toujou achessible dai sit client.',
	'info_boussole_manuelle' => 'Boussola Manuala',
	'info_configurer_boussole' => 'Aqueu fourmulari vi permete de counfigurà l’afichage de la boussola en chausissant lu sit da afichà o noun e l’ordre d’afichage en un group. Lu sit noun afichat soun marcat da un fount tratat e una fouònta grìsa.',
	'info_declarer_boussole_manuelle' => 'Estou fourmulari vi permete de declarà una boussola manuala oustelada per estou sit Un còu declarada, la boussola devendrà achessible per lu sit client qu’utilison aquestou servidou.',
	'info_importer_boussole' => 'Aquela oupcioun vi permete d’empourtà toui lu sit d’una boussola instalada soubre lou vouòstre sit. Se d’unu sit de la boussola chausida soun jà referençat, noun seràn encara creat mà li siéu dounada seràn sincrounisadi embé aqueli fournidi da la boussola per aquestu sit.',
	'info_liste_aucun_hebergement' => 'Mìnga boussola es encara oustelada soubre estou servidou. Utilisàs lou fourmulari aquì souta da declarà una boussola manuala o ativàs un plugin de boussola soubre estou sit.',
	'info_nom_serveur' => 'Sesissès lou noum qu’augurès douna au vouòstre servidou de boussola. Lou noum « spip » es reservat au servidou d’URL « http://boussole.spip.net » e noun pòu pi estre utilisat.', # MODIF
	'info_rubrique_parent' => 'Da creà lu sit de la boussola devès chausì una rùbrica d’acuèlh.',
	'info_site_boussole' => 'Aqueu sit fa partit de la boussola :',
	'info_site_boussoles' => 'Aqueu sit fa partit dei boussola :',
	'info_url_serveur' => 'Sesissès l’URL dóu sit servidou.',

	// L
	'label_1_boussole' => '@nb@ boussola',
	'label_1_site' => '@nb@ sit',
	'label_a_class' => 'Classa de l’àncoura engloubant lou logou',
	'label_activite_serveur' => 'Ativà la founcioun servidou ?',
	'label_actualise_le' => 'Atualisada lou',
	'label_affiche' => 'Afichat ?',
	'label_afficher_descriptif' => 'Afichà lou descritiéu dei sit ?',
	'label_afficher_lien_accueil' => 'Afichà l’estac vers la pàgina d’acuèlh ?',
	'label_afficher_slogan' => 'Afichà l’eslougan dei sit ?',
	'label_alias_boussole' => 'Alias de la boussola',
	'label_ariane_separateur' => 'Separatour :',
	'label_boussole' => 'Boussola da afichà',
	'label_cartouche_boussole' => 'Afichà lou cartoucha de la boussola ?',
	'label_demo' => 'Retroubès la pàgina de demo d’aquesta boussola a l’adressa',
	'label_descriptif' => 'Descritiéu',
	'label_div_class' => 'Classa dóu div engloubant',
	'label_div_id' => 'Id dóu div engloubant',
	'label_langue_site' => 'Per li dounada traduchi, empourtà que la traducioun en la lenga dóu sit.',
	'label_li_class' => 'Classa de cada balisa li de la lista',
	'label_logo' => 'Logou',
	'label_max_articles' => 'Noumbre massimoum d’article afichat per sit',
	'label_max_sites' => 'Noumbre massimoum de sit',
	'label_mode' => 'Chausissès una boussola',
	'label_mode_standard' => '« @boussole@ », boussola ouficial dei sit SPIP',
	'label_modele' => 'Moudel d’afichage',
	'label_n_boussoles' => '@nb@ boussola',
	'label_n_sites' => '@nb@ sit',
	'label_nom' => 'Noum',
	'label_nom_serveur' => 'Noum dóu servidou',
	'label_p_class' => 'Classa dóu paràgrafou engloubant lou descritiéu',
	'label_publier_import' => 'Publicà automaticamen lu sit nouvéu. L’estatut dei sit qu’esiston denant l’emport noun seràn moudificat',
	'label_sepia' => 'Code de la coulou sèpia (sensa #)',
	'label_slogan' => 'Eslougan',
	'label_taille_logo' => 'Talha massimala dóu logou (en pixel)',
	'label_taille_logo_boussole' => 'Talha massimala dóu logou de la boussola (en pixel)',
	'label_taille_titre' => 'Talha massimala dóu titre d’una boussola',
	'label_titre_actualite' => 'Afichà lou titre dóu bloc d’actualità ?', # MODIF
	'label_titre_boussole' => 'Afichà lou titre de la boussola ?',
	'label_titre_groupe' => 'Afichà lou titre dóu group ?',
	'label_titre_site' => 'Afichà lou titre dei sit ?',
	'label_type_bulle' => 'Infourmacioun afichada en la boufa de cada estac',
	'label_type_description' => 'Descricioun afichada a coustà dóu logou',
	'label_ul_class' => 'Classa de la balisa ul de la lista',
	'label_url' => 'URL',
	'label_url_serveur' => 'URL dóu servidou',
	'label_version' => 'Versioun',

	// M
	'message_nok_0_site_importe' => 'Mìnga sit noun es estat empourtat a partì de la boussola @boussole@.',
	'message_nok_alias_boussole_manquant' => 'L’alias de la boussola noun es estat fournit au servidou « @serveur@ ».',
	'message_nok_aucune_boussole_hebergee' => 'Mìnga boussola es encara oustelada soubre lou servidou « @serveur@ ».',
	'message_nok_boussole_inconnue' => 'Mìnga boussola courrespouònde a  l’alias « @alias@ ».',
	'message_nok_boussole_non_hebergee' => 'La boussola « @alias@ » noun es oustelada soubre lou servidou « @serveur@ ».',
	'message_nok_cache_boussole_indisponible' => 'Lou fichié amagadou de la boussola « @alias@ » noun es dispounible soubre lou servidou « @serveur@ ».',
	'message_nok_cache_liste_indisponible' => 'Lou fichié amagadou de la lista dei boussola noun es dispounible soubre lou servidou « @serveur@ ».',
	'message_nok_declaration_boussole_xml' => 'La boussola manuala « @boussole@ » noun pòu estre declarada perqué lou siéu fichié XML es  introuvable.',
	'message_nok_ecriture_bdd' => 'Errour d’escritura en basa de dounada (taula @table@).',
	'message_nok_reponse_invalide' => 'La respouòsta dóu servidou « @serveur@ » es mau fourmada o l’URL sesida noun courrespouònde a un servidou atiéu.', # MODIF
	'message_ok_1_site_importe' => 'Basta un sit es estat empourtat a partì de la boussola @boussole@.',
	'message_ok_boussole_actualisee' => 'La boussola « @fichier@ » es estada messa a jou.',
	'message_ok_boussole_ajoutee' => 'La boussola « @fichier@ » es estat ajustada.',
	'message_ok_boussole_manuelle_ajoutee' => 'La boussola manuala « @boussole@ » es estada declarada au servidou e lu amagadou soun estat mes a jou.',
	'message_ok_n_sites_importes' => '@nb@ sit soun estat empourtat a partì de la boussola @boussole@.',
	'message_ok_serveur_ajoute' => 'Lou servidou « @serveur@ » es estat ajustat (@url@).',
	'modele_boussole_liste_avec_logo' => 'Lista d’estac embé noum, logou e descritiéu',
	'modele_boussole_liste_par_groupe' => 'Lista dei estac testual per group',
	'modele_boussole_liste_simple' => 'Lista simpla d’estac testual',
	'modele_boussole_panorama' => 'Galerìa dei logou',
	'modele_boussole_panorama_sepia' => 'Galerìa dei logou embé efet sèpia',

	// O
	'onglet_client' => 'Founcioun Client', # MODIF
	'onglet_configuration' => 'Counfiguracioun dóu plugin',
	'onglet_serveur' => 'Founcioun Servidou', # MODIF
	'option_aucune_description' => 'Mìnga descricioun',
	'option_descriptif_site' => 'Descritiéu dóu sit',
	'option_nom_site' => 'Noum dóu sit',
	'option_nom_slogan_site' => 'Noum et eslougan dóu sit',
	'option_slogan_site' => 'Eslougan dóu sit',

	// T
	'titre_boite_autres_boussoles' => 'Autri boussola',
	'titre_boite_infos_boussole' => 'BOUSSOLA D’ALIAS',
	'titre_boite_logo_boussole' => 'LOGOU DE LA BOUSSOLA',
	'titre_form_ajouter_boussole' => 'Ajustà o metre a jou una boussola',
	'titre_form_ajouter_serveur' => 'Declarà un servidou de boussola',
	'titre_form_configurer_serveur' => 'Counfigurà la founcioun servidou', # MODIF
	'titre_formulaire_configurer' => 'Counfiguracioun de l’afichage de la boussola',
	'titre_liste_boussoles' => 'Lista dei boussola dispounibli a l’afichage',
	'titre_liste_serveurs' => 'Lista dei servidou achessible dau sit',
	'titre_page_boussole' => 'Gestioun dei boussola',
	'titre_page_configurer' => 'Counfiguracioun dóu plugin boussola',
	'titre_page_importer_boussole' => 'Empourtacioun d’una boussola',
	'titre_page_serveurs_boussole' => 'Founcioun Servidou', # MODIF
];
