<?php
/**
 * Ce fichier contient les cas d'utilisation des pipelines.
 *
 * @package SPIP\BOUSSOLE\PIPELINE
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Affichage, dans la fiche d'un objet site, d'un bloc identifiant
 * la boussole à laquelle appartient le site édité.
 *
 * @pipeline affiche_milieu
 *
 * @param array $flux Flux fourni en entrée au pipeline
 *
 * @return array Flux éventuellement complété
 */
function boussole_affiche_milieu(array $flux) : array {
	if (
		isset($flux['args']['exec'], $flux['args']['id_syndic'])
		and ($flux['args']['exec'] === 'site')
		and ($id_syndic = (int) ($flux['args']['id_syndic']))
	) {
		$texte = recuperer_fond(
			'prive/squelettes/inclure/site_boussole',
			['id_syndic' => $id_syndic]
		);
		if ($texte) {
			if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
				$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
			} else {
				$flux['data'] .= $texte;
			}
		}
	}

	return $flux;
}

/**
 * Affiche dans la page d'accueil des sites référencés un raccourci pour importer l'ensemble
 * des sites d'une boussole déjà installée.
 *
 * @pipeline affiche_gauche
 *
 * @param array $flux Flux fourni en entrée au pipeline
 *
 * @return array Flux éventuellement complété
 */
function boussole_affiche_gauche(array $flux) : array {
	if (
		isset($flux['args']['exec'])
		and $flux['args']['exec'] === 'sites'
	) {
		if (sql_allfetsel('valeur', 'spip_meta', ['nom LIKE ' . sql_quote('boussole_infos%')])) {
			$flux['data'] .= recuperer_fond('prive/squelettes/inclure/sites_importer_boussole', []);
		}
	}

	return $flux;
}

/**
 * Permet de déclencher la mise à jour de la boussole si un site de la base appartenant à cette boussole
 * est refusé ou réhabilité si il était déjà refusé.
 *
 * @pipeline post_edition
 *
 * @param array $flux Flux fourni au pipeline
 *
 * @return array Flux d'entrée sans modification.
 */
function boussole_post_edition(array $flux) : array {
	if (
		isset($flux['args']['table'], $flux['args']['id_objet'], $flux['args']['action'])
		and ($table = $flux['args']['table'])
		and ($table === 'spip_syndic')
		and ($id = (int) ($flux['args']['id_objet']))
		and ($action = $flux['args']['action'])
		and ($action === 'instituer')
		and (
			($flux['args']['statut_ancien'] === 'refuse')
			or ($flux['data']['statut'] === 'refuse')
		)
	) {
		// Il faut détecter si le site appartient à une boussole en se basant sur l'url uniquement
		$urls = [];
		$url_site = sql_getfetsel('url_site', 'spip_syndic', 'id_syndic=' . sql_quote($id));
		$urls[] = $url_site;
		$urls[] = (substr($url_site, -1, 1) == '/') ? substr($url_site, 0, -1) : $url_site . '/';
		if ($id_site = sql_getfetsel('id_site', 'spip_boussoles', sql_in('url_site', $urls))) {
			// Le site appartient bien à une boussole et son id dans la boussole est id_site.
			// Il suffit maintenant de mettre à jour son id_syndic en fonction du type de changement de statut
			$id_maj = ($flux['args']['statut_ancien'] == 'refuse') ? $id : 0;
			sql_updateq('spip_boussoles', ['id_syndic' => $id_maj], 'id_site=' . sql_quote($id_site));
		}
	}

	return $flux;
}

/**
 * Déclare la collection boussoles à l'API d'ezrest.
 *
 * @pipeline liste_ezcollection
 *
 * @param array $collections Configuration des collections déjà déclarées.
 *
 * @return array Collections complétées.
 */
function boussole_liste_ezcollection(array $collections) : array {
	// La collection Boussoles
	if (!$collections) {
		$collections = [];
	}

	$collections['boussoles'] = [
		'module' => 'boussole',				// préfixe du plugin utilisateur (obligatoire)
		'cache'  => [					// définition du cache (obligatoire)
			'type'  => 'ezrest',				// - type de cache utilisé 'ezrest' (PHP) ou 'spip' (HTML)
			'duree' => 3600 * 24    			// - durée du cache en secondes
		],
		'filtres'        => [],
		'sans_condition' => true,				// indicateur de calcul des conditions (facultatif)
		'ressource'      => 'aka_boussole'		// nom du champ utilisé pour identifier la ressource (faculatif);
	];

	return $collections;
}
