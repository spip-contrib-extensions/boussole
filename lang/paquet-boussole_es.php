<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-boussole?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// B
	'boussole_description' => 'Instale este plugin en su sitio y tendrá acceso, dentro de sus esqueletos, al conjunto de sitios de la galaxia SPIP usando los modelos, avellanas o la página Z propuestos.
_ Para los amantes del bricolage, puede utilizar también este plugin para mostrar su propia lista de sitios y diseñar sus propias visualizaciones. ',
	'boussole_nom' => 'Brújula',
	'boussole_slogan' => '¡Las buenas direcciones de la galaxia SPIP u otros lugares!',
];
