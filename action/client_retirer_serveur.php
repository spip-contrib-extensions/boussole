<?php
/**
 * Ce fichier contient l'action `client_retirer_serveur` utilisée par un site client pour
 * retirer un serveur donné de la liste des serveurs consultables.
 *
 * @package SPIP\BOUSSOLE\SERVEUR
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Cette action permet au site client de retirer un serveur donné de sa liste des serveurs
 * qu'il est autorisé à interroger (variable de configuration).
 *
 * Cette action est réservée aux webmestres.
 * Elle nécessite un seul argument, l'identifiant du serveur à retirer.
 *
 * @uses serveur_boussole_retirer()
 *
 * @return void
 */
function action_client_retirer_serveur_dist() : void {
	// Securisation et autorisation car c'est une action auteur:
	// -> argument attendu est l'identifiant du serveur
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$serveur = $securiser_action();

	// Verification des autorisations
	if (!autoriser('retirerserveur', 'boussole')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Suppression du serveur connu par son identifiant.
	// -- On ne supprime jamais le serveur "spip"
	include_spip('inc/serveur_boussole');
	serveur_boussole_retirer($serveur);
}
