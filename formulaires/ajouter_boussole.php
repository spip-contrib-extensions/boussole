<?php
/**
 * Ce fichier contient les fonctions de gestion du formulaire d'ajout d'une boussole
 * en base de données d'un site.
 *
 * @package SPIP\BOUSSOLE\CLIENT
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des données : le formulaire propose la liste des boussoles accessibles
 * à partir des serveurs que le site client a déclaré.
 *
 * @uses boussole_lister_disponibilites()
 * @uses serveur_boussole_est_spip()
 *
 * @return array Le tableau des données à charger par le formulaire :
 *               - 'boussole' : l'alias de la boussole choisie
 *               - '_boussoles' : la liste des boussoles accessibles
 */
function formulaires_ajouter_boussole_charger_dist() : array {
	// Récupération des boussole disponibles au chargement
	include_spip('inc/boussole');
	$boussoles = boussole_lister_disponibilites();

	// Identifier si on est sur le site serveur de la boussole SPIP
	include_spip('inc/serveur_boussole');
	$est_site_boussole_spip = serveur_boussole_est_spip();

	// Réorganiser la liste des boussoles "serveur" :
	// - pour la boussole spip:
	//   -- si on est sur le serveur spip, proposer la boussole spip du plugin et pas celle du serveur spip lui-même
	//   -- sinon, ne présenter que la boussole fournie par le serveur spip
	// - pour les autres boussoles, présenter tous les serveurs possibles
	if (!empty($boussoles['serveurs'])) {
		foreach ($boussoles['serveurs'] as $_serveur => $_boussoles) {
			if (!serveur_boussole_est_spip($_serveur)) {
				if (array_key_exists(_BOUSSOLE_ID_BOUSSOLE_SPIP, $_boussoles)) {
					unset($boussoles['serveurs'][$_serveur][_BOUSSOLE_ID_BOUSSOLE_SPIP]);
				}
			} elseif ($est_site_boussole_spip) {
				unset($boussoles['serveurs'][$_serveur][_BOUSSOLE_ID_BOUSSOLE_SPIP]);
			}
		}
	}

	return [
		'boussole'   => _request('boussole'),
		'_boussoles' => $boussoles,
	];
}

/**
 * Exécution de l'action du formulaire : la boussole choisie est chargée en base en utilisant le fournisseur
 * adéquat.
 *
 * @uses boussole_charger()
 * @uses erreur_boussole_expliquer()
 *
 * @return array Tableau retourné par le formulaire contenant toujours un message de bonne exécution ou
 *               d'erreur. L'indicateur editable est toujours à vrai.
 */
function formulaires_ajouter_boussole_traiter_dist() : array {
	// Initialisation du retour de traitement
	$retour = [];

	// Récupération de la boussole choisie et de son fournisseur
	$choix = _request('boussole');
	[$type_fournisseur, $id_fournisseur, $boussole] = explode(':', $choix);
	$fournisseur = [
		'type' => $type_fournisseur,
		'id'   => $id_fournisseur
	];

	// On insere la boussole dans la base
	include_spip('inc/boussole');
	$erreur = boussole_charger($boussole, $fournisseur);

	// Determination des messages de retour
	include_spip('inc/erreur_boussole');
	if ((int) ($erreur['status']) !== 200) {
		$retour['message_erreur'] = erreur_boussole_expliquer($erreur);
		spip_log("Erreur lors du chargement de la boussole {$boussole}", 'boussole' . _LOG_ERREUR);
	} else {
		$retour['message_ok'] = erreur_boussole_expliquer($erreur);
		spip_log("Chargement ok de la boussole {$boussole}", 'boussole' . _LOG_INFO);
	}
	$retour['editable'] = true;

	return $retour;
}
