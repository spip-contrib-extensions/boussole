<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-boussole?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// B
	'boussole_description' => 'Instalàs aqueu plugin e aurès achès, en lu vouòstre esquelètrou, a toui lu sit de la Galassìa SPIP en utilisant
lu moudel, li avelana o la pàgina Z proupausat.
_ Per lu bricoulaire, poudès finda utilisà aqueu plugin da afichà la vouòstra propri lista de sit e counceure lu vouòstre propri afichage.',
	'boussole_nom' => 'Boussola',
	'boussole_slogan' => 'Li bouòni adressa de la galassìa SPIP o de fouòra !',
];
