<?php
/**
 * Ce fichier contient l'ensemble des fonctions de service de gestion de la collection boussoles.
 *
 * @package SPIP\BOUSSOLE\APIREST
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// -----------------------------------------------------------------------
// ---------------------------- CONTEXTE ---------------------------------
// -----------------------------------------------------------------------
/**
 * Détermine si le serveur est capable de répondre aux requêtes SVP.
 * Pour cela on vérifie si le serveur a été activé et qu'il est prêt à répondre.
 *
 * @param array $erreur Tableau initialisé avec les index identifiant l'erreur ou vide si pas d'erreur.
 *                      Les index mis à jour sont uniquement les suivants car les autres sont initialisés par l'appelant :
 *                      - `type`    : identifiant de l'erreur 501, soit `runtime_nok`
 *                      - `element` : type d'objet sur lequel porte l'erreur, soit `serveur`
 *                      - `valeur`  : la valeur du mode runtime
 *
 * @return bool `true` si la valeur est valide, `false` sinon.
 */
function boussoles_api_verifier_contexte(array &$erreur) : bool {
	// Initialise le retour à true par défaut.
	$est_valide = true;

	include_spip('inc/autoriser');
	if (!autoriser('servir', 'boussole')) {
		$erreur['type'] = 'serveur_nok';
		$erreur['element'] = 'actif';
		$erreur['valeur'] = 'non';

		$est_valide = false;
	}

	return $est_valide;
}

// -----------------------------------------------------------------------
// ----------------------- COLLECTION BOUSSOLES --------------------------
// -----------------------------------------------------------------------

/**
 * Récupère la liste boussoles avec toutes les informations possibles y compris leur nom en mode balise multi.
 *
 * @param array $conditions    Conditions à appliquer au select
 * @param array $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                             construire chaque filtre.
 *
 * @return array Tableau des boussoles dont l'index est l'identifiant de la boussole.
 */
function boussoles_collectionner(array $conditions, array $filtres, array $configuration) : array {
	// Extraire les boussoles fournies par le serveur : il suffit de lire les consignations
	include_spip('inc/boussole');
	$boussoles['boussoles'] = boussole_lire_consignation();

	// Fournir l'identifiant du serveur
	include_spip('inc/serveur_boussole');
	$boussoles['serveur']['id'] = serveur_boussole_identifier();

	return $boussoles;
}

/**
 * Retourne la description complète d'une boussole (sites, traductions, logos).
 *
 * @param string $boussole      Identifiant de la boussole.
 * @param array  $filtres       Tableau des critères de filtrage additionnels à appliquer au select.
 * @param array  $configuration Configuration de la collection utile pour savoir quelle fonction appeler pour
 *                              construire chaque filtre.
 *
 * @return array La description de la boussole, de ses sites et toutes les traductions associées.
 */
function boussoles_ressourcer(string $boussole, array $filtres, array $configuration) : array {
	// Initialisation du tableau de la ressource
	include_spip('inc/serveur_boussole');
	$ressource = [
		'sites'    => [],
		'extras'   => [],
		'boussole' => [],
		'serveur'  => [
			'id' => serveur_boussole_identifier()
		]
	];

	// Le format de la réponse doit permettre de simplifier l'utilisation par le client.
	// Par défaut, l'utilisation est l'insertion en base de la boussole récupérée : il faut donc que la réponse
	// coincide avec les fonctions SQL.
	// -- lecture des sites de la boussole
	$from = 'spip_boussoles';
	$trouver_table = charger_fonction('trouver_table', 'base');
	$table = $trouver_table($from);
	$select = array_diff(array_keys($table['field']), ['id_site', 'id_syndic', 'maj']);
	$where = [
		$configuration['ressource'] . '=' . sql_quote($boussole)
	];
	if ($sites = sql_allfetsel($select, $from, $where)) {
		$ressource['sites'] = $sites;
	}

	// -- lecture des extras de la boussole
	$from = 'spip_boussoles_extras';
	$trouver_table = charger_fonction('trouver_table', 'base');
	$table = $trouver_table($from);
	$select = array_diff(array_keys($table['field']), ['maj']);
	if ($extras = sql_allfetsel($select, $from, $where)) {
		$ressource['extras'] = $extras;
	}

	// -- lecture des informations meta de la boussole
	include_spip('inc/boussole');
	$ressource['boussole'] = boussole_lire_consignation($boussole);

	return $ressource;
}
